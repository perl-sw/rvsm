/*
Copyright 2011. All rights reserved.
Institute of Measurement and Control Systems
Karlsruhe Institute of Technology, Germany

This file is part of libelas.
Authors: Andreas Geiger

libelas is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the License, or any later version.

libelas is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
libelas; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301, USA 
*/

// Demo program showing how libelas can be used, try "./elas -h" for help

#include <sstream>
#include <iomanip>
#include <opencv2/opencv.hpp>

#include <iostream>
#include "elas.h"
#include "image.h"

uint16_t bad_point = std::numeric_limits<uint16_t>::min();
uint16_t max_point = std::numeric_limits<uint16_t>::max();
//float right_fx = 7.1885600e+02;    // seq15
//float right_Tx = -3.86144800e+02;  // seq15
float right_fx = 7.07091200e+02; // seq05
float right_Tx = -3.7981450e+02; // seq05
float baseline = -right_Tx / right_fx;
float depth_factor = baseline * right_fx;
float scale_factor = 2000.0;

using namespace std;

// compute disparities of pgm image input pair file_1, file_2
void process (const char* file_1,const char* file_2, const char* file_3) {

  cout << "Processing: " << file_1 << ", " << file_2 << endl;

  // load images
  image<uchar> *I1,*I2;
  I1 = loadPGM(file_1);
  I2 = loadPGM(file_2);

  // check for correct size
  if (I1->width()<=0 || I1->height() <=0 || I2->width()<=0 || I2->height() <=0 ||
      I1->width()!=I2->width() || I1->height()!=I2->height()) {
    cout << "ERROR: Images must be of same size, but" << endl;
    cout << "       I1: " << I1->width() <<  " x " << I1->height() << 
                 ", I2: " << I2->width() <<  " x " << I2->height() << endl;
    delete I1;
    delete I2;
    return;    
  }

  // get image width and height
  int32_t width  = I1->width();
  int32_t height = I1->height();

  // allocate memory for disparity images
  const int32_t dims[3] = {width,height,width}; // bytes per line = width
  float* D1_data = (float*)malloc(width*height*sizeof(float));
  float* D2_data = (float*)malloc(width*height*sizeof(float));

  // process
  Elas::parameters param;
  param.postprocess_only_left = false;
  Elas elas(param);
  elas.process(I1->data,I2->data,D1_data,D2_data,dims);

  // find maximum disparity for scaling output disparity images to [0..255]
  /*float disp_max = 0;
  for (int32_t i=0; i<width*height; i++) {
    if (D1_data[i]>disp_max) disp_max = D1_data[i];
    if (D2_data[i]>disp_max) disp_max = D2_data[i];
  }

  // copy float to uchar
  image<uchar> *D1 = new image<uchar>(width,height);
  image<uchar> *D2 = new image<uchar>(width,height);
  for (int32_t i=0; i<width*height; i++) {
    D1->data[i] = (uint8_t)max(255.0*D1_data[i]/disp_max,0.0);
    D2->data[i] = (uint8_t)max(255.0*D2_data[i]/disp_max,0.0);
  }*/

  cv::Mat out_depth_image = cv::Mat(height, width, CV_16UC1);
  uint16_t * out_depth_image_data = reinterpret_cast<uint16_t*>(&out_depth_image.data[0]);
  for (int32_t i=0; i<width*height; i++) {
    float disp = D1_data[i];
    //out_depth_image_data[i] = disp;
    if (depth_factor/disp >= 50.0 || depth_factor/disp <= 0.0)
      out_depth_image_data[i] = bad_point;
    else{
      if (depth_factor/disp * scale_factor >= 65535.0)
        out_depth_image_data[i] = max_point;
      else
        out_depth_image_data[i] = (uint16_t)(depth_factor/disp * scale_factor);
    }
  }
  cv::imwrite(file_3, out_depth_image);

  // save disparity images
  /*char output_1[1024];
  char output_2[1024];
  strncpy(output_1,file_1,strlen(file_1)-4);
  strncpy(output_2,file_2,strlen(file_2)-4);
  output_1[strlen(file_1)-4] = '\0';
  output_2[strlen(file_2)-4] = '\0';
  strcat(output_1,"_disp.pgm");
  strcat(output_2,"_disp.pgm");
  savePGM(D1,output_1);
  savePGM(D2,output_2);*/

  // free memory
  delete I1;
  delete I2;
  //delete D1;
  //delete D2;
  //free(D1_data);
  //free(D2_data);
}

int main (int argc, char** argv) {

  // Process kitti sequence 15 data
  std::string left_path = "/home/ganlu/Datasets/05/image_00/";
  std::string right_path = "/home/ganlu/Datasets/05/image_11/";
  std::string result_path = "/home/ganlu/Datasets/05/depth_image/";

  // Only for test images
  //int image_list[] = {0, 3, 5, 6, 9, 10, 12, 15, 18, 20, 21, 24, 25, 27, 30, 33, 35, 36, 39,
  //  40, 42, 45, 48, 50, 78};

  for (int i = 0; i < 801; i++) {
    std::stringstream ss;
    ss << std::setw(6) << std::setfill('0') << i;
    std::string left_image_name = left_path + ss.str() + ".pgm";
    std::string right_image_name = right_path + ss.str() + ".pgm";
    std::string depth_image_name = result_path + ss.str() + ".png";
    process(left_image_name.c_str(), right_image_name.c_str(), depth_image_name.c_str());
  }

  // run demo
  /*if (argc==2 && !strcmp(argv[1],"demo")) {
    process("img/cones_left.pgm",   "img/cones_right.pgm");
    process("img/aloe_left.pgm",    "img/aloe_right.pgm");
    process("img/raindeer_left.pgm","img/raindeer_right.pgm");
    process("img/urban1_left.pgm",  "img/urban1_right.pgm");
    process("img/urban2_left.pgm",  "img/urban2_right.pgm");
    process("img/urban3_left.pgm",  "img/urban3_right.pgm");
    process("img/urban4_left.pgm",  "img/urban4_right.pgm");
    cout << "... done!" << endl;

  // compute disparity from input pair
  } else if (argc==3) {
    process(argv[1],argv[2]);
    cout << "... done!" << endl;

  // display help
  } else {
    cout << endl;
    cout << "ELAS demo program usage: " << endl;
    cout << "./elas demo ................ process all test images (image dir)" << endl;
    cout << "./elas left.pgm right.pgm .. process a single stereo pair" << endl;
    cout << "./elas -h .................. shows this help" << endl;
    cout << endl;
    cout << "Note: All images must be pgm greylevel images. All output" << endl;
    cout << "      disparities will be scaled such that disp_max = 255." << endl;
    cout << endl;
  }*/

  return 0;
}


