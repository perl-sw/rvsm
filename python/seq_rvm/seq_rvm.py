import numpy as np


class SeqRegressionRVM:
    def __init__(self, input, output):
        self.PARAMETER = None
        self.HYPERPARAMETER = None
        self.DIAGNOSTIC = None
        self.basisWidth = 0.05  # NB: data is in [0, 1]
        # Number of iterations
        self.iterations = 500
        # Private attributes
        self.__BasisScales = None
        self.__BASIS = None
        self.__Alpha = None
        self.__beta = None
        self.__X = input        # input matrix
        self.__Targets = output
        self.__Mu = None
        self.__SIGMA = None
        self.__M_full = None    # number of basis
        self.__M = None         # number of survived basis
        self.__N = None         # number of measurements
        self.__Gamma = None     # Well - determinedness factors
        self.__logML = None     # Log marginal likelihood

        self.__PHI = None
        self.__Phi = None
        self.__BASIS_PHI = None
        self.__BASIS_Targets = None
        self.__BASIS_B_PHI = None
        """Sparsity factor - related to how orthogonal a given basis 
        function is to the currently used set of basis functions"""
        self.__S_in = None
        """Quality factor - related to how well the basis function 
        contributes to reducing the error"""
        self.__Q_in = None
        """Sparsity factor with that basis excluded: equations(23)"""
        self.__S_out = None
        """Quality factor with that basis excluded: equations(23)"""
        self.__Q_out = None
        self.__Factor = None    # Relevance factor
        self.__Used = None      # Used basis index
        self.__deltaLogMarginal = None
        self.__nu = None
        self.__newAlpha = None
        self.__SIGMANEW = None
        self.__UPDATE_REQUIRED = None
        self.__selectedAction = None
        self.__UpdateIteration = None
        self.__anyToAdd = None
        self.__anyToDelete = None
        self.__anyWorthwhileAction = None
        # A "reasonable" initial value for the noise in the Gaussian case
        self.__GAUSSIAN_SNR_INIT = 0.1
        # "Reasonable" initial alpha bounds
        self.__INIT_ALPHA_MAX = 1e3
        self.__INIT_ALPHA_MIN = 1e-3

        self.__Aligned_out = None
        self.__Aligned_in = None
        self.__alignDeferCount = 0

        """Define parameters which influence the underlying operation
        of the SparseBayes inference algorithm"""

        """TOLERANCES Any Q ^ 2 - S "relevance factor" less than 
        this is considered to be zero"""
        self.__ZeroFactor = 1e-12
        """If the change in log - alpha for the best re-estimation is 
        less than this, we consider termination"""
        self.__MinDeltaLogAlpha = 1e-3
        """In the Gaussian case, we also require a beta update to
        change the value of log - beta(inverse noise variance) less
        than this to terminate"""
        self.__MinDeltaLogBeta = 1e-6

        """ADD / DELETE 
        - preferring addition where possible will
        probably make the algorithm a little slower and perhaps less
        "greedy"
        - preferring deletion may make the model a little more sparse and the
        algorithm may run slightly quicker
        Note: both these can be set to 'true' at the same time, in which
        case both take equal priority over re - estimation."""
        self.__PriorityAddition = False
        self.__PriorityDeletion = True

        """(GAUSSIAN) NOISE
        When to update the noise estimate 
        The number of iterations from the start for which we update it every
        iteration(to get in the right ball - park to begin with)"""
        self.__BetaUpdateStart = 10

        """After the above, we only regularly update it after a given number
        of iterations"""
        self.__BetaUpdateFrequency = 5

        """Prevent zero - noise estimate(perfect fit) problem - effectively says the 
        noise variance estimate is clamped to be no lower than variance 
        - of - targets / BetaMaxFactor."""
        self.__BetaMaxFactor = 1e6

        """POSTERIOR MODE
        How many alpha updates to do in between each full posterior mode 
        computation in the non 
        - Gaussian case: In principle, this should be set to one(to update the
        posterior every iteration) but it may be more efficient to do several
        alpha updates before re - finding the posterior mode."""
        self.__PosteriorModeFrequency = 1

        """REDUNDANT BASIS
        Check for basis vector alignment / correlation redundancy """
        self.__BasisAlignmentTest = True
        self.__ALIGNMENT_ZERO = 1e-3
        """If BasisAlignmentTest is true, any basis vector with inner product more
        than MAX_ALIGNMENT with any existing model vector will not be added"""
        self.__AlignmentMax = 1 - 1e-3  # 1 - ALIGNMENT_ZERO

        """ACTION CODES: Assign an integer code to the basic action types"""
        self.__ACTION_REESTIMATE = 0
        self.__ACTION_ADD = 1
        self.__ACTION_DELETE = -1

        """Some extra types"""
        self.__ACTION_TERMINATE = 10
        self.__ACTION_NOISE_ONLY = 11
        self.__ACTION_ALIGNMENT_SKIP = 12

        """Initialise automatically approximately according to 'SNR'"""
        self.__beta = 1 / ((np.maximum(1e-6, np.std(output)) * self.__GAUSSIAN_SNR_INIT) ** 2)
        # Squared Exponential kernel
        self.__BASIS = np.exp(-self.dist_squared(input, input) / (self.basisWidth ** 2))

        """Preprocess basis: Compute "lengths" of basis vectors(columns of BASIS)"""
        self.__BasisScales = np.sqrt(sum(self.__BASIS ** 2))

        """Work - around divide - by - zero inconvenience"""
        self.__BasisScales[self.__BasisScales == 0] = 1

        """Normalise each basis vector to 'unit length'"""
        for m in range(self.__BASIS.size[1]):
            self.__BASIS[:, m] = self.__BASIS[:, m] / self.__BasisScales[m]

        """Mu will be analytically calculated later in the Gaussian case"""
        self.__Mu = None

        """Set initial basis to be the largest projection with the targets""" 
        proj = (self.__BASIS.transpose() * self.__Targets)
        self.__Used = np.argmax(abs(proj))
        self.__PHI = self.__BASIS[:, self.__Used]

        """"Exact for single basis function case (diag irrelevant), heuristic 
        in the multiple case"""
        p = np.diag(self.__PHI.transpose() * self.__PHI) * self.__beta
        q = (self.__PHI.transpose() * self.__Targets) * self.__beta
        self.__Alpha = (p ** 2) / (q ** 2 - p)
        self.__Alpha[self.__Alpha < 0] = self.__INIT_ALPHA_MAX

        """"It will be computationally advantageous to "cache" this quantity in 
        the Gaussian case"""
        self.__BASIS_PHI = self.__BASIS.transpose() * self.__PHI
        self.__BASIS_Targets = self.__BASIS.transpose() * self.__Targets

        [self.__N, self.M_full] = self.__BASIS.shape
        self.__M = self.__PHI.shape[1]

        """If we're doing basis alignment testing, we'll need to maintain lists of
        those functions that are near identical, both in and out of the current model."""
        if self.__BasisAlignmentTest:
            self.__Aligned_out = np.array([])
            self.__Aligned_in = np.array([])
            self.__alignDeferCount = 0

        """Initialise with a full explicit computation of the statistics"""
        self.__compute_statistics()

    def __compute_statistics(self):
        """Posterior is analytic: use linear algebra here
        Compute posterior covariance SIGMA(inverse Hessian) via Cholesky decomposition"""
        U = np.linalg.cholesky(np.matmul(self.__PHI.transpose(), np.matmul(
            self.__PHI, self.__beta) + np.diag(self.__Alpha)))
        Ui = np.linalg.solve(U, np.identity(U.shape[0]))
        self.__SIGMA = np.matmul(Ui, Ui.transpose())
        # Posterior mean Mu
        self.__Mu = np.matmul(np.matmul(self.__SIGMA, np.matmul(
            self.__PHI.transpose(), self.__Targets)), self.__beta)
        # Data error and likelihood
        y = np.matmul(self.__PHI, self.__Mu)
        e = (self.__Targets - y)
        ed = np.matmul(e, e)
        data_likely = 0.5 * (self.__N * np.log(self.__beta) - self.__beta * ed)

        # Compute log marginal likelihood
        logdet_hess_over_2 = np.sum(np.log(np.diag(U)), axis=0)
        self.logML = data_likely - np.matmul(
            np.transpose(self.__Mu ** 2), 0.5 * self.__Alpha) + 0.5 * np.sum(
            np.log(self.__Alpha), axis=0) - logdet_hess_over_2

        # Well-determinedness factors
        self.__Gamma = 1 - self.__Alpha * np.sum(Ui ** 2, axis=1)

        """COMPUTE THE Q & S VALUES
        Q: "quality" factor - related to how well the basis function contributes
        to reducing the error 
        S: "sparsity factor" - related to how orthogonal a given basis function
        is to the currently used set of basis functions""" 

        # Gaussian case simple: beta a scalar
        self.__BASIS_B_PHI = np.matmul(self.__beta, self.__BASIS_PHI)

        """The S_in calculation exploits the fact that BASIS is normalised,
        i.e. sum(BASIS ** 2, 1) == ones(1, M_full)"""
        self.__S_in = self.__beta - np.sum(np.matmul(self.__BASIS_B_PHI, Ui) ** 2, axis=1)
        self.__Q_in = np.matmul(
            self.__beta, self.__BASIS_Targets - np.matmul(self.__BASIS_PHI, self.__Mu))
        self.__S_out = self.__S_in
        self.__Q_out = self.__Q_in

        # S, Q with that basis excluded: equations(23)
        self.__S_out[self.__Used] = \
            (self.__Alpha * self.__S_in[self.__Used]) / (self.__Alpha - self.__S_in[self.__Used])
        self.__Q_out[self.__Used] = \
            (self.__Alpha * self.__Q_in[self.__Used]) / (self.__Alpha - self.__S_in[self.__Used])
        # Pre - compute the "relevance factor" for ongoing convenience
        self.__Factor = self.__Q_out * self.__Q_out - self.__S_out

    def __decision_phase(self):
        """DECISION PHASE
        Assess all potential actions
        Compute change in likelihood for all possible updates"""
        DeltaML = np.zeros((self.__M_full, 1))
        Action = self.__ACTION_REESTIMATE * np.ones((self.__M_full, 1)) # Default

        # 'Relevance Factor'(Q ^ S - S) values for basis functions in model
        UsedFactor = self.__Factor[self.__Used]

        # RE-ESTIMATION: must be a POSITIVE 'factor' and already IN the model
        iu = UsedFactor > self.__ZeroFactor
        index = self.__Used[iu]
        NewAlpha = self.__S_out[index] ** 2. / self.__Factor[index]
        Delta = (1. / NewAlpha - 1. / self.__Alpha[iu]) # Temp vector

        # Quick computation of change in log - likelihood given all re - estimations
        DeltaML[index] = (Delta * (self.__Q_in[index] ** 2) / (
            Delta * self.__S_in[index] + 1) - np.log(
            1 + self.__S_in(index) * Delta)) / 2

        """DELETION: if NEGATIVE factor and IN model
        But don't delete:
            - any "free" basis functions(e.g.the "bias")
            - if there is only one basis function(M=1)
        
        (In practice, this latter event ought only to happen with the Gaussian
        likelihood when initial noise is too high.In that case, a later beta
        update should 'cure' this.)"""
        iu = ~iu   # iu = UsedFactor <= ZeroFactor
        index = self.__Used[iu]
        self.__anyToDelete = (index.size != 0) and self.__M > 1

        if self.__anyToDelete:
            # Quick computation of change in log - likelihood given all deletions
            DeltaML[index] = -(self.__Q_out[index] ** 2) / (
                self.__S_out[index] + self.__Alpha[iu]) - np.log(
                1 + self.__S_out[index] / self.__Alpha[iu]) / 2
            Action[index] = self.__ACTION_DELETE
            # Note: if M == 1, DeltaML will be left as zero, which is fine

        """ADDITION: must be a POSITIVE factor and OUT of the model
        Find ALL good factors..."""
        GoodFactor = self.__Factor > self.__ZeroFactor
        # ... then mask out those already in model
        GoodFactor[self.__Used] = 0
        # ... and then mask out any that are aligned with those in the model
        if self.__BasisAlignmentTest:
            GoodFactor[self.__Aligned_out] = 0

        index = np.where(GoodFactor)
        self.__anyToAdd = index.size != 0
        if self.__anyToAdd:
            # Quick computation of change in log - likelihood given all additions
            quot = (self.__Q_in[index] ** 2) / self.__S_in[index]
            DeltaML[index] = (quot - 1 - np.log(quot)) / 2
            Action[index] = self.__ACTION_ADD

        return DeltaML, Action

    def __postprocess_action(self, DeltaML, Action):
        """Post-process action results to take account of preferences
                If we prefer ADD or DELETE actions over RE-ESTIMATION"""
        if (self.__anyToAdd and self.__PriorityAddition) or (self.__anyToDelete and self.__PriorityDeletion):
            """We won't perform re-estimation this iteration, which we achieve by zero-ing
            out the delta"""
            DeltaML[Action == self.__ACTION_REESTIMATE] = 0
            """Furthermore, we should enforce ADD if preferred and DELETE is not
            - and vice versa"""
            if self.__anyToAdd and self.__PriorityAddition or not self.__PriorityDeletion:
                DeltaML[Action == self.__ACTION_DELETE] = 0

            if self.__anyToDelete and self.__PriorityDeletion and not self.__PriorityAddition:
                DeltaML[Action == self.__ACTION_ADD] = 0

        # Finally...we choose the action that results in the greatest change in likelihood
        self.__deltaLogMarginal = np.max(DeltaML)
        self.__nu = np.argmax(DeltaML)
        self.__selectedAction = Action[self.__nu]
        self.__anyWorthwhileAction = self.__deltaLogMarginal > 0

        """We need to note if basis self.__nu is already in the model, and if so, % find its
        interior index, denoted by 'j' """
        j = None
        if self.__selectedAction == self.__ACTION_REESTIMATE or self.__selectedAction == self.__ACTION_DELETE:
            j = np.where(self.__Used == self.__nu)

        """Get the individual basis vector for update and compute its optimal alpha,
        according to equation(20): alpha = S_out ^ 2 / (Q_out ^ 2 - S_out)"""
        self.__Phi = self.__BASIS[:, self.__nu]
        self.__newAlpha = (self.__S_out[self.__nu] ** 2) / self.__Factor[self.__nu]

        return j

    def __alignment_check(self):
        """ALIGNMENT CHECKS: If we're checking "alignment", we may have further processing to do on
        addition and deletion"""
        if self.__BasisAlignmentTest:
            """Addition - rule out addition(from now onwards) if the new basis vector is aligned
            too closely to one or more already in the model"""
            if self.__selectedAction == self.__ACTION_ADD:
                # Basic test for correlated basis vectors (note, Phi and columns of PHI are normalised)
                p = np.matmul(self.__Phi.transpose(), self.__PHI)
                findAligned = np.where(p > self.__AlignmentMax)
                numAligned = findAligned.size
                if numAligned > 0:
                    # The added basis function is effectively indistinguishable from one present already
                    self.__selectedAction = self.__ACTION_ALIGNMENT_SKIP
                    self.__alignDeferCount = self.__alignDeferCount + 1
                    """Make a note so we don't try this next time
                    May be more than one in the model, which we need to note was the cause of function
                    'self.__nu' being rejected"""
                    self.__Aligned_out = np.append(self.__Aligned_out, [self.__nu * ones(numAligned, 1)])
                    self.__Aligned_in = np.append(self.__Aligned_in, [self.__Used[findAligned]])

                # Deletion: reinstate any previously deferred basis functions resulting from this basis function
                if self.__selectedAction == self.__ACTION_DELETE:
                    findAligned = np.where(self.__Aligned_in == self.__nu)
                    numAligned = findAligned.size
                    if numAligned > 0:
                        self.__Aligned_in = np.delete(self.__Aligned_out, findAligned, axis=0)
                        self.__Aligned_out = np.delete(self.__Aligned_out, findAligned, axis=0)

    def __action_phase(self, j):
        """We'll want to note if we've made a change which necessitates later updating
        of the statistics"""
        self.__UPDATE_REQUIRED = False

        if self.__selectedAction == self.__ACTION_REESTIMATE:
            """Basis function 'self.__nu' is already in the model, and we're re-estimating its 
            corresponding alpha - should correspond to Appendix A .3"""
            oldAlpha = self.__Alpha[j]
            self.__Alpha[j] = self.__newAlpha
            s_j = self.__SIGMA[:, j]
            deltaInv = 1 / (self.__newAlpha - oldAlpha)
            kappa = 1 / (self.__SIGMA[j, j] + deltaInv)
            tmp = kappa * s_j
            self.__SIGMANEW = self.__SIGMA - tmp * s_j.transpose()
            deltaMu = -self.__Mu[j] * tmp
            self.__Mu = self.__Mu + deltaMu
            if self.__UpdateIteration:
                self.__S_in = self.__S_in + kappa * np.matmul(self.__BASIS_B_PHI, s_j) ** 2
                self.__Q_in = self.__Q_in - np.matmul(self.__BASIS_B_PHI, deltaMu)

            self.__UPDATE_REQUIRED = True

        elif self.__selectedAction == self.__ACTION_ADD:
            """Basis function self.__nu is not in the model, and we're adding it in 
            - should correspond to Appendix A .2"""
            BASIS_Phi = np.matmul(self.__BASIS.transpose(), self.__Phi)
            self.__BASIS_PHI = np.stack((self.__BASIS_PHI, BASIS_Phi), axis=-1)
            B_Phi = self.__beta * self.__Phi
            BASIS_B_Phi = self.__beta * BASIS_Phi

            tmp = np.transpose(np.matmul(np.matmul(B_Phi.transpose(), self.__PHI), self.__SIGMA))
            self.__Alpha = np.vstack((self.__Alpha, self.__newAlpha))
            self.__PHI = np.stack((self.__PHI, self.__Phi), axis=-1)
            s_ii = 1 / (self.__newAlpha + self.__S_in[self.__nu])
            s_i = -s_ii * tmp
            TAU = -np.matmul(s_i, tmp.transpose())
            self.__SIGMANEW = np.vstack((np.stack((
                self.__SIGMA + TAU, s_i), axis=-1), np.stack((s_i.transpose(), s_ii), axis=-1)))
            mu_i = s_ii * self.__Q_in[self.__nu]
            deltaMu = np.append(-mu_i * tmp, [mu_i])
            self.__Mu = np.append(self.__Mu, [0]) + deltaMu

            if self.__UpdateIteration:
                mCi = BASIS_B_Phi - np.matmul(self.__BASIS_B_PHI, tmp)
                self.__S_in = self.__S_in - s_ii * mCi ** 2
                self.__Q_in = self.__Q_in - mu_i * mCi

            self.__Used = np.append(self.__Used, [self.__nu])
            self.__UPDATE_REQUIRED = True

        elif self.__selectedAction == self.__ACTION_DELETE:
            """Basis function self.__nu is in the model, but we're removing it
            - should correspond to Appendix A.4"""
            self.__BASIS_PHI = np.delete(self.__BASIS_PHI, j, axis=1)
            self.__PHI = np.delete(self.__PHI, j, axis=1)
            self.__Alpha = np.delete(self.__Alpha, j, axis=0)

            s_jj = self.__SIGMA[j, j]
            s_j = self.__SIGMA[:, j]
            tmp = s_j / s_jj
            self.__SIGMANEW = self.__SIGMA - np.matmul(tmp, s_j.transpose())
            self.__SIGMANEW = np.delete(self.__SIGMANEW, j, axis=0)
            self.__SIGMANEW = np.delete(self.__SIGMANEW, j, axis=1)
            deltaMu = - self.__Mu[j] * tmp
            mu_j = self.__Mu[j]
            self.__Mu = self.__Mu + deltaMu
            self.__Mu = np.delete(self.__Mu, j, axis=0)

            if self.__UpdateIteration:
                jPm    = np.matmul(self.__BASIS_B_PHI, s_j)
                self.__S_in    = self.__S_in + jPm ** 2 / s_jj
                self.__Q_in    = self.__Q_in + np.matmul(jPm, mu_j) / s_jj

            self.__Used = np.delete(self.__Used, j, axis=0)
            self.__UPDATE_REQUIRED = true

        self.__M = self.__Used.size

    def __update_statistics(self, i):
        """UPDATE STATISTICS:
        If we've performed a meaningful action, update the relevant variables"""
        if self.__UPDATE_REQUIRED:
            """S_in & Q_in values were calculated earlier
            Here: update the S_out / Q_out values and relevance factors"""
            if self.__UpdateIteration:
                # Previous "update" statisics calculated earlier are valid
                self.__S_out = self.__S_in
                self.__Q_out = self.__Q_in
                tmp = self.__Alpha / (self.__Alpha - self.__S_in[self.__Used])
                self.__S_out[self.__Used] = tmp * self.__S_in[self.__Used]
                self.__Q_out[self.__Used] = tmp * self.__Q_in[self.__Used]
                self.__Factor = (self.__Q_out * self.__Q_out - self.__S_out)
                self.__SIGMA = self.__SIGMANEW
                self.__Gamma = 1 - self.__Alpha * np.diag(self.__SIGMA)
                self.__BASIS_B_PHI = self.__beta * self.__BASIS_PHI

            self.__logML = self.__logML + self.__deltaLogMarginal

        """GAUSSIAN NOISE ESTIMATE:
        For Gaussian likelihood, re-estimate noise variance if:
        - not fixed, AND
        - an update is specified this cycle as normal, OR
         - we're considering termination"""
        if self.__selectedAction == self.__ACTION_TERMINATE or \
                        i <= self.__BetaUpdateStart or np.remainder(i, self.__BetaUpdateFrequency) == 0:
            betaZ1 = self.__beta
            y = np.matmul(self.__PHI, self.__Mu)
            e = (self.__Targets - y)
            self.__beta = (self.__N - np.sum(self.__Gamma, axis=0)) / np.matmul(e.transpose(), e)
            # Work - around zero - noise issue
            self.__beta = np.minimum(self.__beta, self.__BetaMaxFactor / np.var(self.__Targets))
            deltaLogBeta = log(self.__beta) - log(betaZ1)

            if abs(deltaLogBeta) > self.__MinDeltaLogBeta:
                # Full re-computation of statistics required after beta update
                self.__compute_statistics()

                if self.__selectedAction == self.__ACTION_TERMINATE:
                    """We considered terminating above as no alpha update seemed
                    worthwhile. However, a beta update has made a non-trivial
                    increase in the likelihood, so we continue."""
                    self.__selectedAction = self.__ACTION_NOISE_ONLY

    def run(self):
        i = 0   # Iteration number
        last_iteration = False

        while not last_iteration:
            i = i + 1
            self.__UpdateIteration = 1
            # Decide what to do
            [DeltaML, Action] = self.__decision_phase()

            # Post - process action results to take account of preferences
            j = self.__postprocess_action(DeltaML, Action)

            """TERMINATION CONDITIONS:
            Propose to terminate if:
            1. there is no worthwhile(likelihood - increasing) action, OR
            2a. the best action is an ACTION_REESTIMATE but this would only lead to an
                infinitesimal alpha change, AND
            2b. at the same time there are no potential awaiting deletions"""
            if not self.__anyWorthwhileAction or \
                    (self.__selectedAction == self.__ACTION_REESTIMATE and
                                 abs(np.log(self.__newAlpha) - np.log(self.__Alpha[j])) < self.__MinDeltaLogAlpha and
                             not self.__anyToDelete):
                self.__selectedAction = self.__ACTION_TERMINATE

            # alignment check
            self.__alignment_check()

            # Implement above decision
            self.__action_phase(j)

            # update variables
            self.__update_statistics(i)

            # Check for "natural" termination
            iteration_limit = i == self.__iterations
            last_iteration = iteration_limit

        """OUTPUT VARIABLES:
        We also choose to sort here - it can't hurt and may help later"""
        index = self.__Used.argsort()
        self.__Used.sort()
        self.__PARAMETER.Relevant = self.__Used

        # Not forgetting to correct for normalisation too
        self.__PARAMETER.Value = self.__Mu[index] / np.transpose(self.__BasisScales[self.__Used[index]])

        self.__HYPERPARAMETER.Alpha = self.__Alpha[index] / (np.transpose(self.__BasisScales[self.__Used[index]]) ** 2)
        self.__HYPERPARAMETER.beta = self.__beta

        self.__DIAGNOSTIC.Gamma = self.__Gamma[index]
        self.__DIAGNOSTIC.iterations = i
        self.__DIAGNOSTIC.S_Factor = self.__S_out
        self.__DIAGNOSTIC.Q_Factor = self.__Q_out
        self.__DIAGNOSTIC.M_full = self.__M_full

    @staticmethod
    def dist_squared(x, y):
        nx = x.shape[0]
        ny = y.shape[0]
        return np.sum((x ** 2), axis=1) * np.ones((1, ny)) + np.ones(
            (nx, 1)) * np.transpose(np.sum((y ** 2), axis=1)) - 2 * np.matmul(x, y.transpose())
