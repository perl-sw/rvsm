clc;

% adding the dev folder to thr path
cd('../data/data_kitti/15/pc_global/')
addpath(genpath(pwd))

% adding the dev folder to thr path
cd('../../../../matlab')
addpath(genpath(pwd))

% shuffling the random number generator seed
rng('shuffle')