function cmap = NCLTcolormap(label)
% This functions takes a vector of labels and return the corresponding RGB
% color row per semantic label in KITTI dataset.

% class_name = [r, g, b, label];
background  = [200/255,       200/255,       200/255,       0];
water       = [ 30/255, 144/255, 255/255, 1];
road        = [255/255, 255/255, 255/255, 2];
sidewalk    = [128/255,  64/255, 128/255, 3];
terrain     = [128/255, 128/255,   0/255, 4];
building    = [255/255, 128/255,   0/255, 5];
vegetation  = [107/255, 142/255,  35/255, 6];
car         = [0,       0,       142/255, 7];
person      = [220/255,  20/255,  60/255, 8];
bike        = [119/255,  11/255,  32/255, 9];
pole        = [192/255, 192/255, 192/255, 10];
stair       = [123/255, 104/255, 238/255, 11];
traffic_sign = [255/255, 255/255, 0,       12];
sky         = [135/255, 206/255, 235/255, 13];



% building = [0.5020, 0, 0, 1];
% vegetation = [0.5020, 0.5020, 0, 2];
% car = [0.2510, 0, 0.5020, 3];
% road = [0.5020, 0.2510, 0.5020, 4];
% fence = [0.2510, 0.7529, 0, 5];
% sidewalk = [0, 0, 0.7529, 6];
% pole = [0, 0.2510, 0.2510, 7];
% two classes different in results
% wall = [64, 64, 128, 5];
% post = [192, 192, 128, 7];

% make a kd-tree oject for NN queries
X = [background;
    water;
    road
    sidewalk;
    terrain;
    building;
    vegetation;
    car;
    person;
    bike;
    pole;
    stair;
    traffic_sign;
    sky];
MdlKDT = KDTreeSearcher((1:13)', 'Distance','cityblock');


% initialize the colormap
cmap = zeros(length(label), 3);

% fill in the RGB per label
for i = 1:length(label)
    if label(i) == 0
%         cmap(i,:) = [128 128 128]; % grey
        cmap(i,:) = [0 0 0]; % black
    else
        idx = knnsearch(MdlKDT,label(i));   % find the class id
        cmap(i,:) = X(idx,1:3);             % fill in the corresponding RGB
    end
end