function result = KittiSemanticMapCompress(L, points3d, nc, invalid_label)

% % load labeled point cloud as prior
% T = readtable(frame);
% points3d = table2array(T);
% L = points3d(:, end);  % labels
% points3d(:, end) = []; % 3D points

% find valid points
L_valid = L(L~=invalid_label);
points3d_valid = points3d(L~=invalid_label, :);
% L_valid = L;
% points3d_valid = points3d;

% downsample the pointcloud

% method 1: make sure there are enough points from each class kept
r = 0.01; % keep r percent of data
idx_downsampled = [];

lid = unique(L_valid);
for i = 1:length(lid)
    idx = find(L == lid(i));
    reduced_size = max(floor(r * length(idx)), min(50, length(idx)));
    idx_reduced = floor(rand(reduced_size,1) * length(idx)) + 1;
    idx_downsampled = [idx_downsampled; idx(idx_reduced)];
end
X = points3d_valid(idx_downsampled, 1:3);  % only XYZ
y = L_valid(idx_downsampled);

% % method 2: uniformly 
% skip = 150;
% X = points3d_valid(1:skip:end, 1:3);  % only XYZ
% y = L_valid(1:skip:end);


X = (X - mean(X)) ./ std(X);
yl = cell(nc,1);  % initialise target


%% RVM Semantic Map
% covfunc = {@covSEiso}; hyp = []; hyp.cov = log([1.8 4.8]);
covfunc = {@covMaternard, 5}; hyp = []; hyp.cov = log([1.3452 1.2746 1.9771 2.2133]);
% covfunc = {@covMaternard, 5}; hyp = []; hyp.cov = log([1.8096 1.6246 3.9090 4.8987]);
BASIS = feval(covfunc{:}, hyp.cov, X);

% Train and Test
t_points = points3d_valid(:, 1:3);  % query for point cloud
t_points = (t_points - mean(t_points)) ./ std(t_points);

PARAMETER = cell(nc,1);
t0 = tic;
for i = 1 : nc
    positiveLabels = y == i - 1;  % labels 0-10
    yl{i} = double(positiveLabels); 
    disp(['class ', num2str(i), ': ', num2str(sum(yl{i}))])
    
    cla = rvm_classification(X, yl{i}, BASIS);
    cla.run;
     
    % OUTPUT VARIABLES
    PARAMETER{i}			= cla.PARAMETER;
    HYPERPARAMETER{i}		= cla.HYPERPARAMETER;
     
    % compute the inferred prediction function
    relevant_vector{i} = X(PARAMETER{i}.Relevant, :);
    w_infer{i}         = PARAMETER{i}.Value;
    test_BASIS_points  = feval(covfunc{:}, hyp.cov, relevant_vector{i}, t_points)';
    
    y_out_points{i} = test_BASIS_points * w_infer{i};
    y_out_points{i} = 1./(1+exp(-y_out_points{i}));
end
time = toc(t0);

[Y_points, Ymax_points, Yid_points] = softHardLabels(y_out_points);  % Yid is predicted labels

% save xyz distribution
xyzdist = [points3d_valid(:, 1:3) Y_points];
T = table(xyzdist);
writetable(T, 'kitti_15', 'Delimiter', ' ', 'WriteVariableNames',false)

%% Plot results
% predicted map
outcmap = KITTIcolormap(Yid_points);
outmap = pointCloud(points3d_valid(:, 1:3), 'Color', outcmap);
% original map
incmap = KITTIcolormap(L_valid+1);
inmap = pointCloud(points3d_valid(:, 1:3), 'Color', incmap);

% plot RVM output
figure; 
subplot(2,1,1); pcshow(outmap), axis equal tight, axis off, title('rvm points')
subplot(2,1,2); pcshow(inmap), axis equal tight, axis off, title('prior')

% save results
%result.frame = frame;
result.time = time;
%result.roc = roc.roc;
%result.AUCtot = roc.AUCtot;
%result.sensitivityTotal = roc.sensitivityTotal;
%result.accuracyTotal = roc.accuracyTotal;
%result.F1Total = roc.F1Total;
% result.map = map;
result.relevant_vector = relevant_vector;
result.w_infer = w_infer;
% result.rgbData = rgbData;
result.parameters = PARAMETER;
result.hyperparameters = HYPERPARAMETER;
%result.gtLabel = gtLabel;