function result = NCLTSemanticMapPrediction(frame, nc, result)

% Load input frame
T = readtable(frame);
points3d = table2array(T); % 3D points

% Test points are all valid measured points
t = points3d;
% t = (t - mean(t)) ./ std(t);

%% RVM Semantic Map
% covfunc = {@covMaternard, 5}; hyp = []; hyp.cov = log([1.8096 1.6246 3.9090 4.8987]);
covfunc = {@covMaternard, 5}; hyp = []; hyp.cov = log([3.8096 3.8096 1.9090 0.8]);

% Prediction
t0 = tic;
for i = 1 : nc
    % compute the inferred prediction function
    relevant_vector = result.relevant_vector{i};
    w_infer	= result.w_infer{i};
    test_BASIS = feval(covfunc{:}, hyp.cov, relevant_vector, t)';
    
    y_out{i} = test_BASIS * w_infer;
end
time = toc(t0);

[Y, Ymax, Yid] = softHardLabels(y_out);

%% Plot results
% predicted map
outcmap = NCLTcolormap(Yid);
map = pointCloud(points3d(:,1:3), 'Color', outcmap);

%save xyz distribution
xyzdist = [points3d outcmap Y];
T = table(xyzdist);
writetable(T, erase(frame, ".txt") + '_voxel.txt', 'Delimiter', ' ', 'WriteVariableNames',false)

% %% Plot results
% % predicted map
% outcmap = NCLTcolormap(Yid);
% map = pointCloud(points3d(:,1:3), 'Color', outcmap);

% plot RVM output
figure; pcshow(map), axis equal, axis off

result.frame = frame;
result.time = time;
%result.roc = roc.roc;
%result.AUCtot = roc.AUCtot;
%result.sensitivityTotal = roc.sensitivityTotal;
%result.accuracyTotal = roc.accuracyTotal;
%result.F1Total = roc.F1Total;
result.map = map;
result.Y = Y;
result.points3d = points3d;
%result.rgbData = rgbData;
%result.parameters = PARAMETER;
%result.hyperparameters = HYPERPARAMETER;
%result.gtLabel = gtLabel;