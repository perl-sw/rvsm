function [label, u, v] = KITTIprojectionLabel(points, segimage, M)
% This functions takes 3D points, intrinsic camera matrix, and ground truth 
% segmented image and returns ground truth semantic labels. 

% creating homogeneous points
p = [points'; ones(1, size(points,1))];

A = [1, 0, 0, 0;
     0, 0, 1, 0;
     0,-1, 0, 1;
     0, 0, 0, 1;];
 
pose_23 = [0.993450105,-0.009455221,0.113874875,1.222599030;
        0.008173623,0.999897897,0.011716089,-0.266481787;
        -0.113974035,-0.010708581,0.993425965,31.212371826;
        0,0,0,1];

pose_25 = [0.998757660 -0.010880793 0.048628107 0.440367341;
      0.010121630 0.999823391 0.015830701 -0.178927273;
      -0.048791766 -0.015318837 0.998691499 20.271450043;
      0 0 0 1];
    
% project onto the image plane
% xyw = M * inv(A) *p;
% xyw = M * inv(pose_25) * inv(A) * p;
xyw = M * (A \ p);

% remove the scale
u = round(xyw(1,:) ./ xyw(3,:));
v = round(xyw(2,:) ./ xyw(3,:));

% semantic label RGB
Rs = segimage(:,:,1); Gs = segimage(:,:,2); Bs = segimage(:,:,3);

% image RGB
% R = image(:,:,1); G = image(:,:,2); B = image(:,:,3);

% class_name = [r, g, b] % label
% building    = [128, 0, 0];    % 1
% vegetation  = [128, 128, 0];  % 2
% car         = [64, 0, 128];   % 3
% road        = [128, 64, 128]; % 4
% fence       = [64, 192, 0];   % 5
% sidewalk    = [0, 0, 192];    % 6
% pole        = [0, 64, 64];    % 7
% two classes different in results
% wall = [64, 64, 128, 5];
% post = [192, 192, 128, 7];

% class_name = [r, g, b, label];
building = [128, 0, 0];
sky = [128, 128, 128];
road = [128, 64, 128];
vegetation = [128, 128, 0];
sidewalk = [0, 0, 192];
car = [64, 0, 128];
pedestrain = [64, 64, 0];
cyclist = [0, 128, 192];
signate = [192, 128, 128];
fence = [64, 64, 128];
pole = [192, 192, 128];


% % make a kd-tree oject for NN queries
% X = [building;
%     vegetation;
%     car;
%     road;
%     fence;
%     sidewalk;
%     pole];
% make a kd-tree oject for NN queries
X = [building;
    sky
    road;
    vegetation;
    sidewalk;
    car;
    pedestrain;
    cyclist;
    signate;
    fence;
    pole];
MdlKDT = KDTreeSearcher(X, 'Distance','cityblock');

% initialize the colormap
label = zeros(size(points,1),1);
% image size to check if projected points are in the image frame
[n, m, ~] = size(segimage); 

%RGB = zeros(length(label),3);
% fill in the labels
for i = 1:length(label)
    if u(i) > m || u(i) < 1 || v(i) > n || v(i) < 1 % projected point is outside
        label(i) = 1;   % null class id
%         disp("Projected point is outside of the image frame; assigning the null class id.")
        continue
    else
        rgb = double([Rs(v(i),u(i)), Gs(v(i),u(i)), Bs(v(i),u(i))]); % ground truth semantic RGB
    end
    
    if rgb(1) == 128 && rgb(2) == 128 && rgb(3) == 128 % null class (sky)
        label(i) = 2;
    elseif rgb(1) == 0 && rgb(2) == 0 && rgb(3) == 0 % null class
        label(i) = 2;
    else
        idx = knnsearch(MdlKDT,rgb);    % find the label id
        label(i) = idx;
        %RGB(i,:) = double([R(v(i),u(i)), G(v(i),u(i)), B(v(i),u(i))]); % image RGB;
    end
end
