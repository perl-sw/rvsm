function cmap = KITTIcolormap(label)
% This functions takes a vector of labels and return the corresponding RGB
% color row per semantic label in KITTI dataset.

% class_name = [r, g, b, label];
building = [128/255,  0, 0, 0];
sky = [128/255, 128/255, 128/255, 1];
road = [128/255, 64/255, 128/255, 2];
vegetation = [128/255, 128/255, 0, 3];
sidewalk = [0, 0, 192/255, 4];
car = [64/255, 0, 128/255, 5];
pedestrain = [64/128, 64/128, 0, 6];
cyclist = [0, 128/255, 192/255, 7];
signate = [192/255, 128/255, 128/255, 8];
fence = [64/255, 64/255, 128/255, 9];
pole = [192/255, 192/255, 128/255, 10];



% building = [0.5020, 0, 0, 1];
% vegetation = [0.5020, 0.5020, 0, 2];
% car = [0.2510, 0, 0.5020, 3];
% road = [0.5020, 0.2510, 0.5020, 4];
% fence = [0.2510, 0.7529, 0, 5];
% sidewalk = [0, 0, 0.7529, 6];
% pole = [0, 0.2510, 0.2510, 7];
% two classes different in results
% wall = [64, 64, 128, 5];
% post = [192, 192, 128, 7];

% make a kd-tree oject for NN queries
X = [building;
    sky
    road;
    vegetation;
    sidewalk;
    car;
    pedestrain;
    cyclist;
    signate;
    fence;
    pole];
MdlKDT = KDTreeSearcher((1:11)', 'Distance','cityblock');


% initialize the colormap
cmap = zeros(length(label), 3);

% fill in the RGB per label
for i = 1:length(label)
    if label(i) == 0
%         cmap(i,:) = [128 128 128]; % grey
        cmap(i,:) = [0 0 0]; % black
    else
        idx = knnsearch(MdlKDT,label(i));   % find the class id
        cmap(i,:) = X(idx,1:3);             % fill in the corresponding RGB
    end
end