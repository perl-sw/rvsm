function [Y, Ymax, Yid] = softHardLabels(y)
% This function computes softmax prediction of the probabilistic labels as
% well as arg max computations of the labels to get hard labels.

nc = length(y);     % number of classes
Y = [];
% softmax computation of labels (soft labels)
for i = 1:nc
    if ~isempty(y{i})
        Y = [Y, exp(y{i})];
    end
end


for i = 1:size(Y, 1)
    Y(i, :) = exp(Y(i,:)-max(Y(i,:)));
end

Y = Y ./ sum(Y,2);

% arg max computation of labels (hard labels)
[Ymax, Yid]  = max(Y,[],2);
