% Convert png images to pgm images
% Lu Gan (ganlu@umich.edu)

clear;
clc;
close all

% Set Paths
image_path = '/home/ganlu/Datasets/05/';
left_image_path = strcat(image_path, 'image_0/');
right_image_path = strcat(image_path, 'image_1/');
pgm_left_image_path = strcat(image_path, 'image_00/');
pgm_right_image_path = strcat(image_path, 'image_11/');


file_path = 'image_0/';

% Get all image names
file_list = dir(strcat(image_path, file_path, '*.png'));
file_number = length(file_list);

% Traverse the image list
for i = 1 : 801
    
    file_name = file_list(i).name;
    
    % Read stereo images
    left_image_name = strcat(left_image_path, file_name);
    left_image = imread(left_image_name);
    
    right_image_name = strcat(right_image_path, file_name);
    right_image = imread(right_image_name);
    
    pgm_left_image_name = strcat(pgm_left_image_path, file_name(1:end-4), '.pgm');
    pgm_right_image_name = strcat(pgm_right_image_path, file_name(1:end-4), '.pgm');
    
    imwrite(left_image, pgm_left_image_name);
    imwrite(right_image, pgm_right_image_name);
end



