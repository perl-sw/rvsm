% Script to build RVM semantic maps using KITTI sequence 15 test se
clc; clear; close all

% seq_folder = "/home/ganlu/Datasets/nclt/prior_nclt_octree (copy)/";
seq_folder = "/home/ganlu/Datasets/nclt/rvm_nclt_octree (copy)/"
file_pattern = fullfile(seq_folder, '*.txt');
files = dir(file_pattern);

% class_number = 14;
% invalid_label = 13;  % sky
% result = cell(size(files));
% 
% for i = 1:length(files)
%     result{i} = NCLTSemanticMap(seq_folder+files(i).name, class_number, invalid_label);
% end


dist = [];
gtlabel = [];
for i = 1:length(files)
    i
    T = readtable(seq_folder + files(i).name);
    data = table2array(T);
    dist = [dist; data(:, 4:end-1);];
    gtlabel = [gtlabel; data(:, end) + 1];
end

roc = multiClassROCanalysis(dist, gtlabel)