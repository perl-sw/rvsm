% Script to build RVM semantic maps using KITTI sequence 15 test set
clc; clear; close all

seq_folder = "/home/ganlu/Datasets/nclt/octree_centroids/";
file_pattern = fullfile(seq_folder, '*.txt');
files = dir(file_pattern);

class_number = 14;
invalid_label = 13;  % sky
% result = cell(size(files));

L_All = [];
points3d_All = [];
for i = 1 : length(files)
%     result{i} = NCLTSemanticMap(seq_folder+files(i).name, class_number, invalid_label);
    frame = seq_folder+files(i).name;
    % load labeled point cloud as prior
    T = readtable(frame);
    points3d = table2array(T);
    L = points3d(:, end);  % labels
    points3d(:, end) = []; % 3D points
    L_All = [L_All; L];
    points3d_All = [points3d_All; points3d];
end

result = NCLTSemanticMapCompress(L_All, points3d_All, class_number, invalid_label);