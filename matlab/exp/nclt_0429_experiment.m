% Script to build RVM semantic maps using KITTI sequence 15 test set
clc; clear; close all

seq_folder = "/home/ganlu/Datasets/nclt/segmented_txt/";
file_pattern = fullfile(seq_folder, '*.txt');
files = dir(file_pattern);

class_number = 14;
invalid_label = 13;  % sky
result = cell(size(files));

for i = 1:length(files)
    result{i} = NCLTSemanticMap(seq_folder+files(i).name, class_number, invalid_label);
end