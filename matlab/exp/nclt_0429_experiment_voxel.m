% Script to build RVM semantic maps using KITTI sequence 15 test set
% Script to build RVM semantic maps using KITTI sequence 15 test set
clc; clear; close all

load result.mat

seq_folder = "/home/ganlu/Datasets/nclt/octree_centroids/";
file_pattern = fullfile(seq_folder, '*.txt');
files = dir(file_pattern);

class_number = 14;
map = cell(size(files));
for i = 1:length(files)
    map{i} = NCLTSemanticMapPrediction(seq_folder+files(i).name, class_number, result);
end