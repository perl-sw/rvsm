% Script to build RVM semantic maps using KITTI sequence 15 test set
clc; clear; close all

frames = {'000000.txt'
%           '000001.txt'
%           '000002.txt'
          '000003.txt'
%           '000004.txt'
%           '000005.txt'
          '000006.txt'
%           '000007.txt'
%           '000008.txt'
          '000009.txt'
          '000012.txt'
          '000015.txt'
          '000018.txt'
          '000021.txt'
%           '000022.txt'
%           '000023.txt'
          '000024.txt'
%           '000025.txt'
%           '000026.txt'
          '000027.txt'
%           '000028.txt'
%           '000029.txt'       
          '000030.txt'
%           '000031.txt'
%           '000032.txt'
%           '000033.txt'
%           '000034.txt'
%           '000035.txt'
%           '000036.txt'
%           '000037.txt'
%           '000038.txt'
%           '000039.txt'
%           '000040.txt'
%           '000041.txt'
%           '000042.txt'
%           '000043.txt'
%           '000044.txt'
%           '000045.txt'
%           '000046.txt'
%           '000047.txt'
%           '000048.txt'
%           '000049.txt'
%           '000050.txt'
%           '000051.txt'
%           '000052.txt'
%           '000053.txt'
%           '000054.txt'
%           '000055.txt'
%           '000056.txt'
%           '000057.txt'
%           '000058.txt'
%           '000059.txt'
%           '000060.txt'
%           '000061.txt'
%           '000062.txt'
%           '000063.txt'
%           '000064.txt'
%           '000065.txt'
%           '000066.txt'
%           '000067.txt'
%           '000068.txt'
%           '000069.txt'
%           '000070.txt'
%           '000071.txt'
%           '000072.txt'
%           '000073.txt'
%           '000074.txt'
%           '000075.txt'
%           '000076.txt'
%           '000077.txt'
%           '000078.txt'
%           '000079.txt'
%           '000080.txt'
%           '000081.txt'
%           '000082.txt'
%           '000083.txt'
%           '000084.txt'
%           '000085.txt'
%           '000086.txt'
%           '000087.txt'
%           '000088.txt'
%           '000089.txt'
%           '000090.txt'
};



class_number = 11;
invalid_label = 1;  % sky
% result = cell(size(frames));

L_All = [];
points3d_All = [];
for i = 1:length(frames)
    frame = frames{i};
    % load labeled point cloud as prior
    T = readtable(frame);
    points3d = table2array(T);
    L = points3d(:, end);  % labels
    points3d(:, end) = []; % 3D points
    L_All = [L_All; L];
    points3d_All = [points3d_All; points3d];

end

result = KittiSemanticMapCompress(L_All, points3d_All, class_number, invalid_label);