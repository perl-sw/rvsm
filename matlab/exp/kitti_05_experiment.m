% Script to build RVM semantic maps using KITTI sequence 05 test set
clc; clear; close all

frames = cell(801, 1);

for i = 0 : 800
    str_f = sprintf('%06d', i);
    name = [str_f '.txt'];
    frames{i+1} = name;
end

class_number = 11;
invalid_label = 1;  % sky
result = cell(size(frames));

for i = 653:length(frames)
    result{i} = KittiSemanticMap(frames{i}, class_number, invalid_label);
end