% Example for single scan RVM semantic map
% Lu Gan, ganlu@umiche.edu

%{
class_name       = [r, g, b, label];
1   building     = [128/255,    0,          0,       0];
2   sky          = [128/255,    128/255,    128/255, 1];
3   road         = [128/255,    64/255,     128/255, 2];
4   vegetation   = [128/255,    128/255,    0,       3];
5   sidewalk     = [0,          0,          192/255, 4];
6   car          = [64/255,     0,          128/255, 5];
7   pedestrain   = [64/128,     64/128,     0,       6];
8   cyclist      = [0,          128/255,    192/255, 7];
9   signate      = [192/255,    128/255,    128/255, 8];
10  fence        = [64/255,     64/255,     128/255, 9];
11  pole         = [192/255,    192/255,    128/255, 10];
%}

clc; clear; close all

% load labeled point cloud as prior
T = readtable('15_000000.txt');
points3d = table2array(T);
L = points3d(:, end);  % labels
points3d(:, end) = []; % 3D points

% find valid points
invalid_label = 1;  % sky label
L_valid = L(L~=invalid_label);
points3d_valid = points3d(L~=invalid_label, 1:3);  % only XYZ

% number of classes
nc = 11;
 
% downsample the pointcloud
skip = 50;
X = points3d_valid(1:skip:end, :); 

% initialise target
y = L_valid(1:skip:end);
yl = cell(nc,1);

%% RVM Semantic Map
% Heuristically adjust basis width to account for distance scaling with dimension.
% dimension = size(X,2);
% basisWidth = 0.05;
% basisWidth	= basisWidth^(1 / dimension);		% NB: data is in [0,1]
% Compute squared basis (design) matrix
% BASIS            = 0.1^2 * exp(-distSquared(X,X) / (basisWidth^2));

% covfunc = {@covSEiso}; hyp = []; hyp.cov = log([1.8 4.8]);
% covfunc = {@covSEard}; hyp = []; hyp.cov = log([1.8096 1.6246 3.9090 4.8987]);
% covfunc = {@covMaterniso, 5}; hyp = []; hyp.cov = log([1.8 4.8]);
% covfunc = {@covMaternard, 5}; hyp = []; hyp.cov = log([1.8096 1.6246 3.9090 4.8987]);
covfunc = {@covMaternard, 5}; hyp = []; hyp.cov = log([1.3452 1.2746 1.9771 2.2133]);
BASIS = feval(covfunc{:}, hyp.cov, X);

% Train and Test
t_points = points3d_valid;  % query for point clouds

PARAMETER = cell(nc,1);
relevance_vectors = [];
relevance_vector_labels = [];
t0 = tic;
for i = 1:nc
    positiveLabels = y == i - 1;  % labels: 0-10
    yl{i} = double(positiveLabels); 
    disp(['class ', num2str(i), ': ', num2str(sum(yl{i}))])
    
    cla = rvm_classification(X, yl{i}, BASIS);
    cla.run;
     
    % OUTPUT VARIABLES
    PARAMETER{i}			= cla.PARAMETER;
    HYPERPARAMETER{i}		= cla.HYPERPARAMETER;
    
    relevance_vector{i} = X(PARAMETER{i}.Relevant, :);
    
    relevance_vectors = [relevance_vectors; relevance_vector{i}];
    relevance_vector_labels = [relevance_vector_labels; (i-1)*ones(size(relevance_vector{i}, 1),1)];
    
    w_infer{i}	        = PARAMETER{i}.Value;
    test_BASIS_points         = feval(covfunc{:}, hyp.cov, relevance_vector{i}, t_points)'; 
    
    % Compute the inferred prediction function
    y_out_points{i} = test_BASIS_points * w_infer{i};
end
time = toc(t0);

[Y_points, Ymax_points, Yid_points] = softHardLabels(y_out_points);  % Yid is predicted labels 

t_points_labels = 0*ones(size(X, 1),1);
relevance_vectors = [X; relevance_vectors  ];
relevance_vector_labels = [t_points_labels ; relevance_vector_labels ];





%% Plot results
rvcmap = KITTIcolormap(relevance_vector_labels + 1);
rvmap  = pointCloud(relevance_vectors, 'Color', rvcmap);
% predicted map
outcmap = KITTIcolormap(Yid_points);
outmap = pointCloud(t_points(:, 1:3), 'Color', outcmap);
% original map
incmap = KITTIcolormap(L_valid + 1);
inmap = pointCloud(t_points(:, 1:3), 'Color', incmap);

% plot RVM output
figure; pcshow(outmap), axis equal tight, axis off
figure;pcshow(inmap), axis equal tight, axis off
pcshow(outmap, 'MarkerSize', 100), axis equal tight, axis off