% Toy example for RVM Semantic Map
clc; clear; close all
fsize = 20; % font size
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

%% load data
load('nyu_v2_965.mat')

%% downsample labels
% method 1: make sure there are enough points from each class kept
r = 0.01; % keep r percent of data
labelMask = 0*L;
lid = unique(L); lid(1) = [];
for i = 1:length(lid)
    idx = find(L == lid(i));
    reduced_size = min(max(floor(r * length(idx)), min(10, length(idx))), 150);    
    idx_reduced = floor(rand(reduced_size,1) * length(idx)) + 1;    
    labelMask(idx(idx_reduced)) = 1;
end
L = L .* labelMask;

% method 2: uniformly
% r = 10;
% labelMask = 0*L;
% labelMask(1:r:end, 1:r:end) = 1;
% L = L.*labelMask;

%% convert labels to RGB colours
RGB = label2rgb(L);
R = RGB(:,:,1); G = RGB(:,:,2); B = RGB(:,:,3);

%% set a unique colour per class ID
[classID, IL, IC] = unique(L);
classID(1) = []; IL(1) = [];    % remove null class id
classCol = [R(IL), G(IL), B(IL)];     % unique class colours

%% convert depth to 3D point cloud using camera parameterts
depth = double(depth);
points3d = depth_plane2depth_world(depth);

% plot labeled point cloud
cmap = [R(:), G(:), B(:)];
ptCloud = pointCloud(points3d, 'Color', cmap);
pcshow(ptCloud), axis equal
title('NYU labeled point cloud'), xlabel('x'), ylabel('y'), zlabel('z')

%% Training points are labeled points
l = L(:); lid = find(l~=0);
skip = 1; % downsample the pointcloud
X = points3d(lid(1:skip:end),:); y = (l(lid(1:skip:end)));

% Standardize data
X = (X - mean(X)) ./ std(X);
% Test points are all measured points
t = points3d;

%% RVM Semantic Map
% number of classes
lq = L(IL); nc = length(lq);
yl = cell(nc,1);

basisWidth = 0.05;		% NB: data is in [0,1]

% Heuristically adjust basis width to account for 
% distance scaling with dimension.
dimension = 3;
basisWidth = basisWidth^(1/dimension);

% Set test basis
% test = t; % test data
test = (t - mean(t)) ./ std(t); % standardized test data
nx = size(X,1);
nt = size(test,1);
D2 = (sum((test.^2), 2) * ones(1,nx)) + (ones(nt, 1) * sum((X.^2),2)') - ...
    2*test*X';
test_BASIS = exp(-D2/(basisWidth^2));
M = size(test_BASIS, 2);
    
tic
for i = 1:nc
    positiveLabels = y == lq(i);
    yl{i} = double(positiveLabels); 
    disp(['class ', num2str(i), ': ', num2str(sum(yl{i}))])
    
    %% RVM
    classifier = rvm_classification(X, yl{i});
    classifier.run;

    %% OUTPUT VARIABLES
    PARAMETER			= classifier.PARAMETER;
    HYPERPARAMETER		= classifier.HYPERPARAMETER;
    
    %% Test
    w_infer	= zeros(M, 1);
    w_infer(PARAMETER.Relevant)	= PARAMETER.Value;
    

    % Compute the inferred prediction function
    y_out{i} = test_BASIS * w_infer;    
end
toc

% softmax computation of labels (soft labels)
Y = []; 
for i = 1:nc
    Y = [Y, exp(y_out{i})];
end
Y = Y ./ sum(Y,2);

% arg max computation of labels (hard labels)
[Ymax,Yid]  = max(Y,[],2);
outcmap = classCol(Yid, :);

% expected label colour using all basis (doesn't work well!)
% eR = sum(Y .* repmat(double(classCol(:,1))', size(Y,1), 1), 2);
% eG = sum(Y .* repmat(double(classCol(:,1))', size(Y,1), 1), 2);
% eB = sum(Y .* repmat(double(classCol(:,1))', size(Y,1), 1), 2);
% ecmap = [eR, eG, eB];
% MdlKDT = KDTreeSearcher(double(classCol), 'Distance', 'cityblock');
% IdxKDT = knnsearch(MdlKDT,ecmap);
% outcmap = classCol(IdxKDT, :);

map = pointCloud(t, 'Color', outcmap);

% plot GPC output
figure
pcshow(map), axis equal
title('RVM Semantic Map'), xlabel('x'), ylabel('y'), zlabel('z')

% read ground truth
gtL = zeros(size(IC,1), size(IL,1));
for i = 1:size(IC,1)
    gtL(i, IC(i)) = 1;
end
gtL(:,1) = [];      % remove null class column

% ROC analysis
roc = cell(size(gtL,2),1);
AUCtot = 0;
for i = 1:size(gtL,2)
    roc{i} = ROCanalysis(Y(:,i), gtL(:,i));
    AUCtot = AUCtot  + roc{i}.auc;
    disp(roc{i}.auc)
end
AUCtot = AUCtot ./ size(gtL,2);
disp(['AUCtot = ', num2str(AUCtot)])