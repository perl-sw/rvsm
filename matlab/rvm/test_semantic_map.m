% Example for single scan RVM semantic map
% Lu Gan, ganlu@umiche.edu

%{
class_name       = [r, g, b, label];
1   building     = [128/255,    0,          0,       0];
2   sky          = [128/255,    128/255,    128/255, 1];
3   road         = [128/255,    64/255,     128/255, 2];
4   vegetation   = [128/255,    128/255,    0,       3];
5   sidewalk     = [0,          0,          192/255, 4];
6   car          = [64/255,     0,          128/255, 5];
7   pedestrain   = [64/128,     64/128,     0,       6];
8   cyclist      = [0,          128/255,    192/255, 7];
9   signate      = [192/255,    128/255,    128/255, 8];
10  fence        = [64/255,     64/255,     128/255, 9];
11  pole         = [192/255,    192/255,    128/255, 10];
%}

clc; clear; close all

% load labeled point cloud as prior
T = readtable('15_000000.txt');
points3d = table2array(T);
L = points3d(:, end);  % labels
points3d(:, end) = []; % 3D points

% find valid points
invalid_label = 1;  % sky label
L_valid = L(L~=invalid_label);
points3d_valid = points3d(L~=invalid_label, 1:3);  % only XYZ

% number of classes
nc = 11;
 
% downsample the pointcloud
skip = 50;
X = points3d_valid(1:skip:end, :); 

% initialise target
y = L_valid(1:skip:end);
yl = cell(nc,1);

%% RVM Semantic Map
% Heuristically adjust basis width to account for distance scaling with dimension.
% dimension = size(X,2);
% basisWidth = 0.05;
% basisWidth	= basisWidth^(1 / dimension);		% NB: data is in [0,1]
% Compute squared basis (design) matrix
% BASIS            = exp(-distSquared(X,X) / (basisWidth^2));

% covfunc = {@covSEiso}; hyp = []; hyp.cov = log([1.8 4.8]);
% covfunc = {@covSEard}; hyp = []; hyp.cov = log([1.8096 1.6246 3.9090 4.8987]);
% covfunc = {@covMaterniso, 5}; hyp = []; hyp.cov = log([1.8 4.8]);
% covfunc = {@covMaternard, 5}; hyp = []; hyp.cov = log([1.8096 1.6246 3.9090 4.8987]);
% covfunc = {@covSEiso}; hyp = []; hyp.cov = log([1.3416 2.1909]);
% covfunc = {@covSEard}; hyp = []; hyp.cov = log([1.8096 1.6246 3.9090 4.8987])/2;
% covfunc = {@covMaterniso, 5}; hyp = []; hyp.cov = log([1.8 4.8])/2;
covfunc = {@covMaternard, 5}; hyp = []; hyp.cov = log([1.3452 1.2746 1.9771 2.2133]);
BASIS = feval(covfunc{:}, hyp.cov, X);

% Train and Test
t_points = points3d_valid;  % query for point clouds
T = readtable('15_000000_voxel');  % query for voxel centroids
t_voxels = table2array(T);


PARAMETER = cell(nc,1);
t0 = tic;
for i = 1:nc
    positiveLabels = y == (i - 1);  % labels: 0-10
    yl{i} = double(positiveLabels); 
    disp(['class ', num2str(i-1), ': ', num2str(sum(yl{i}))])
    
    if sum(yl{i}) == 0
        % -1000 to make probability zero in softmax
        y_out_points{i} = -1000 * ones(size(t_points,1),1);
        y_out_voxels{i} = -1000 * ones(size(t_voxels,1),1);
        continue
    end
    
    cla = rvm_classification(X, yl{i}, BASIS);
    cla.run;
     
    % OUTPUT VARIABLES
    PARAMETER{i}			= cla.PARAMETER;
    HYPERPARAMETER{i}		= cla.HYPERPARAMETER;
    
    relevance_vector{i} = X(PARAMETER{i}.Relevant, :);
    w_infer{i}	        = PARAMETER{i}.Value;
    test_BASIS_points         = feval(covfunc{:}, hyp.cov, relevance_vector{i}, t_points)'; 
    test_BASIS_voxels         = feval(covfunc{:}, hyp.cov, relevance_vector{i}, t_voxels)';
    
    % Compute the inferred prediction function
    y_out_points{i} = test_BASIS_points * w_infer{i};
    y_out_voxels{i} = test_BASIS_voxels * w_infer{i};
%     y_out_points{i} = 1./(1+exp(-y_out_points{i}));
end
time = toc(t0)

[Y_points, Ymax_points, Yid_points] = softHardLabels(y_out_points);  % Yid is predicted labels
[Y_voxels, Ymax_voxels, Yid_voxels] = softHardLabels(y_out_voxels);  

%% Evaluate
% Read baseline results
T = readtable('15_000000_prior.txt');
points_prior = table2array(T);
T = readtable('15_000000_crf.txt');
points_crf = table2array(T);

% Load left camera intrinsic matrix
T = readtable('15_calib.txt', 'Delimiter', 'space', 'ReadRowNames', true, 'ReadVariableNames', false);
A = table2array(T);
P2 = vertcat(A(1, 1:4), A(1, 5:8), A(1, 9:12));  % P2 is the calib for color cam0
        
% Get gt labels
gtImage = imread('15_000000.png');
gtLabel = KITTIprojectionLabel(t_points(:,1:3), gtImage, P2);
gtPriorLabel = KITTIprojectionLabel(points_prior(:, 1:3), gtImage, P2);
gtCrfLabel = KITTIprojectionLabel(points_crf(:, 1:3), gtImage, P2);
gtVoxelLabel = KITTIprojectionLabel(t_voxels(:, 1:3), gtImage, P2);

% ROC analysis
roc_prior = multiClassROCanalysis(points_prior(gtPriorLabel~=2,4:end), gtPriorLabel(gtPriorLabel~=2));
roc_crf = multiClassROCanalysis(points_crf(gtCrfLabel~=2,4:end), gtCrfLabel(gtCrfLabel~=2));
roc_points = multiClassROCanalysis(Y_points(gtLabel~=2,:), gtLabel(gtLabel~=2));
roc_voxels = multiClassROCanalysis(Y_voxels(gtVoxelLabel~=2,:), gtVoxelLabel(gtVoxelLabel~=2));

%% Plot results
% predicted map
outcmap = KITTIcolormap(Yid_points);
outmap = pointCloud(t_points(:, 1:3), 'Color', outcmap);
% predicted voxel map
outvoxelcmap = KITTIcolormap(Yid_voxels);
outvoxelmap = pointCloud(t_voxels(:, 1:3), 'Color', outvoxelcmap);
% original map
incmap = KITTIcolormap(L_valid + 1);
inmap = pointCloud(t_points(:, 1:3), 'Color', incmap);
% gt map
gtcmap = KITTIcolormap(gtLabel);
gtmap = pointCloud(t_points(:, 1:3), 'Color', gtcmap);

% plot RVM output
figure; 
subplot(2,2,1); pcshow(outmap), axis equal tight, axis off, title('rvm points')
subplot(2,2,2); pcshow(outvoxelmap), axis equal tight, axis off, title('rvm voxel')
subplot(2,2,3); pcshow(inmap), axis equal tight, axis off, title('prior')
subplot(2,2,4); pcshow(gtmap), axis equal tight, axis off, title('ground truth')

% print evaluation result
method = {'2D CNN';'CRF';'RVM-points';'RVM-voxels'};
Accuracy = [roc_prior.accuracyTotal; roc_crf.accuracyTotal; roc_points.accuracyTotal; roc_voxels.accuracyTotal];
Precision = [roc_prior.precisionTotal; roc_crf.precisionTotal; roc_points.precisionTotal; roc_voxels.precisionTotal];
IoU = [roc_prior.IoUtot; roc_crf.IoUtot; roc_points.IoUtot; roc_voxels.IoUtot];
AUC = [roc_prior.AUCtot; roc_crf.AUCtot; roc_points.AUCtot; roc_voxels.AUCtot];

T = table(method, Accuracy, Precision, IoU, AUC);
disp(T)
