classdef rvm_classification < handle
    % Relevance Vector Machine Binary Classification
    % Sequential inference based on Mike Tipping MATLAB code
    
    % PUBLIC PROPERTIES
    properties
        PARAMETER;
        HYPERPARAMETER;
        DIAGNOSTIC;
        % Length-scale (basiswidth in the kernel)
        dimension;
        basisWidth	= 0.05;		% NB: data is in [0,1]
        % Number of iterations
        iterations	= 1000;
    end
    
    % PRIVATE PROPERTIES
    properties(Access = private)
        BasisScales;
        BASIS;
        Alpha;
        beta;
        X; % inpuut matrix
        Targets;
        Mu;
        SIGMA;
        U;          % Cholesky decomposition of the posterior covariance
        M_full;     % number of basis
        M;          % number of survived basis
        N;          % number of measurements
        Gamma;      % Well-determinedness factors
        logML;      % Log marginal likelihood
        
        PHI;
        Phi;
        BASIS_PHI;
        BASIS_Targets;
        BASIS_B_PHI;
        S_in = [];          % Sparsity factor - related to how orthogonal a given basis function is to the currently used set of basis functions
        Q_in = [];          % Quality factor - related to how well the basis function contributes to reducing the error
        S_out = [];         % Sparsity factor with that basis excluded: equations (23)
        Q_out = [];         % Quality factor with that basis excluded: equations (23)
        Factor = [];        % Relevance factor
        Used = [];          % Used basis index
        deltaLogMarginal;
        nu;
        newAlpha;
        SIGMANEW;
        UPDATE_REQUIRED;
        selectedAction;
        UpdateIteration;
        anyToAdd;
        anyToDelete;
        anyWorthwhileAction;
        % A "reasonable" initial value for the noise in the Gaussian case
        GAUSSIAN_SNR_INIT	= 0.1;
        % "Reasonable" initial alpha bounds
        INIT_ALPHA_MAX	= 1e3;
        INIT_ALPHA_MIN	= 1e-3;
        
        Aligned_out		= [];
        Aligned_in		= [];
        alignDeferCount	= 0;
    end
    
    % PRIVATE CONSTANTS
    properties(Access = private, Constant)
        % Define parameters which influence the underlying operation of the
        % SparseBayes inference algorithm
        
        % TOLERANCES
        %
        % Any Q^2-S "relevance factor" less than this is considered to be zero
        %
        ZeroFactor			= 1e-12;
        %
        % If the change in log-alpha for the best re-estimation is less than this,
        % we consider termination
        %
        MinDeltaLogAlpha	= 1e-3;
        %
        % In the Gaussian case, we also require a beta update to change the value
        % of log-beta (inverse noise variance) less than this to terminate
        %
        MinDeltaLogBeta	= 1e-6;
        
        % ADD/DELETE
        %
        % - preferring addition where possible will probably make the algorithm a
        % little slower and perhaps less "greedy"
        %
        % - preferring deletion may make the model a little more sparse and the
        % algorithm may run slightly quicker
        %
        % Note: both these can be set to 'true' at the same time, in which case
        % both take equal priority over re-estimation.
        %
        PriorityAddition	= false;
        PriorityDeletion	= true;
        
        % (GAUSSIAN) NOISE
        %
        % When to update the noise estimate
        %
        % The number of iterations from the start for which we update it every
        % iteration (to get in the right ball-park to begin with)
        %
        BetaUpdateStart		= 10;
        %
        % After the above, we only regularly update it after
        % a given number of iterations
        %
        BetaUpdateFrequency	= 5;
        %
        % Prevent zero-noise estimate (perfect fit) problem
        % -	effectively says the noise variance estimate is clamped to be no
        %	lower than variance-of-targets / BetaMaxFactor.
        %
        BetaMaxFactor			= 1e6;
        
        % POSTERIORMODE
        %
        % How many alpha updates to do in between each full posterior mode
        % computation in the non-Gaussian case
        %
        % In principle, this should be set to one (to update the posterior every
        % iteration) but it may be more efficient to do several alpha updates before
        % re-finding the posterior mode.
        %
        PosteriorModeFrequency	= 1;
        
        % REDUNDANT BASIS
        %
        % Check for basis vector alignment/correlation redundancy
        %
        BasisAlignmentTest		= true;
        %
        ALIGNMENT_ZERO					= 1e-3;
        %
        % If BasisAlignmentTest is true, any basis vector with inner product more
        % than MAX_ALIGNMENT with any existing model vector will not be added
        %
        AlignmentMax			= 1 - 1e-3; % 1 - ALIGNMENT_ZERO
        
        % ACTION CODES
        % Assign an integer code to the basic action types
        ACTION_REESTIMATE	= 0;
        ACTION_ADD			= 1;
        ACTION_DELETE		= -1;
        % Some extra types
        ACTION_TERMINATE		= 10;
        ACTION_NOISE_ONLY		= 11;
        ACTION_ALIGNMENT_SKIP	= 12;
    end
    
    methods(Static)
        function D2 = distSquared(X,Y)
            nx	= size(X,1);
            ny	= size(Y,1);
            %
            D2 = (sum((X.^2), 2) * ones(1,ny)) + (ones(nx, 1) * sum((Y.^2),2)') - ...
                2*X*Y';
        end
        %
        function y = sigmoid(x)
            y = 1 ./ (1+exp(-x));
        end
    end
    
    methods
        % Class constructor
        function obj = rvm_classification(input, output, basis)
            obj.X = input;
            obj.Targets = output;
            % Heuristically adjust basis width to account for distance scaling with dimension.
            obj.dimension = size(obj.X,2); 
            obj.basisWidth	= obj.basisWidth^(1 / obj.dimension);		% NB: data is in [0,1]
            % Squared Exponential kernel
            %obj.BASIS = exp(-obj.distSquared(input, input) / (obj.basisWidth^2));
            obj.BASIS = basis;
            % preprocess basis
            % Compute "lengths" of basis vectors (columns of BASIS)
            obj.BasisScales	= sqrt(sum(obj.BASIS.^2));
            % Work-around divide-by-zero inconvenience
            obj.BasisScales(obj.BasisScales == 0)	= 1;
            % Normalise each basis vector to "unit length"
            for m = 1:size(obj.BASIS,2)
                obj.BASIS(:,m) = obj.BASIS(:,m) / obj.BasisScales(m);
            end
            % Mu will be analytically calculated later in the Gaussian case
            obj.Mu		= [];
            % Set initial basis to be the largest projection with the targets
            % First, compute 'linearised' output for use in heuristic initialisation
            TargetsPseudoLinear	= (2 * obj.Targets - 1);
            proj			= (obj.BASIS' * TargetsPseudoLinear);
            [~, obj.Used]	= max(abs(proj));
            obj.PHI	= obj.BASIS(:,obj.Used);
            
            % Heuristic initialisation based on log-odds
            LogOut	= (TargetsPseudoLinear*0.9+1)/2;
            obj.Mu	= obj.PHI \ (log(LogOut./(1-LogOut)));
            
            % Heuristic initialisation, trapping Mu==0
            obj.Alpha	= 1./(obj.Mu + (obj.Mu==0)).^2;
            % Limit max/min alpha
            obj.Alpha(obj.Alpha < obj.INIT_ALPHA_MIN)	= obj.INIT_ALPHA_MIN;
            obj.Alpha(obj.Alpha > obj.INIT_ALPHA_MAX)	= obj.INIT_ALPHA_MAX;
            
            [obj.N, obj.M_full]	= size(obj.BASIS);
            obj.M = size(obj.PHI,2);
            
            % If we're doing basis alignment testing, we'll need to maintain lists of
            % those functions that are near identical, both in and out of the current
            % model.
            if obj.BasisAlignmentTest
                obj.Aligned_out		= [];
                obj.Aligned_in		= [];
                obj.alignDeferCount	= 0;
            end
            
            % Initialise with a full explicit computation of the statistics
            obj.logML = obj.compute_statistics();
        end
        %
        function [likelihoodMode, badHess] = posterior_mode(obj)
            % TOLERANCES
            % Termination criterion for each gradient dimension
            GRADIENT_MIN	= 1e-6;
            
            % Minimum fraction of the full Newton step considered
            STEP_MIN	= 1/(2^8);
            A		= diag(obj.Alpha);
            
            % NB: for historical reasons, we work in term of error here (negative
            % log-liklihood) and minimise
            
            % Get current model output and data error
            BASIS_Mu		= obj.PHI * obj.Mu; % Linear output
            [dataError, y]	= obj.data_error(BASIS_Mu);
            
            % Add on the weight penalty
            regulariser		= (obj.Alpha' * (obj.Mu.^2))/2;
            newTotalError	= dataError + regulariser;
            
            % Add on the weight penalty
            itsMax = 25;    % Max iteration; more than enough
            badHess		= false;
            errorLog	= zeros(itsMax,1);
            
            for iteration = 1:itsMax 
                % Log the error value each iteration
                errorLog(iteration)	= newTotalError;
                
                % Construct the gradient
                e	= (obj.Targets - y);
                g	= obj.PHI' * e - obj.Alpha .* obj.Mu;
                
                % Compute the likelihood-dependent analogue of the noise precision.
                % NB: Beta now a vector.
                obj.beta	= y .* (1-y);
                
                % Compute the Hessian
                BASIS_B	= obj.PHI .* (obj.beta * ones(1, size(obj.PHI,2)));
                H		= (BASIS_B' * obj.PHI + A);
                % Invert Hessian via Cholesky, watching out for ill-conditioning
                [obj.U, pdErr]	= chol(H);
                % Make sure its positive definite
                if pdErr
                    % If you see this, it's *probably* the result of a bad choice of
                    % basis. e.g. a kernel with too large a "width"
                    badHess			= true;
                    obj.U			= [];
                    obj.beta		= [];
                    likelihoodMode	= [];
                    return
                end
                
                % Before progressing, check for termination based on the gradient norm
                if all(abs(g) < GRADIENT_MIN)
                    break
                end
                
                % If all OK, compute full Newton step: H^{-1} * g
                DeltaMu	= obj.U \ (obj.U' \ g);
                step		= 1;
                
                while step > STEP_MIN
                    % Follow gradient to get new value of parameters
                    Mu_new		= obj.Mu + step * DeltaMu;
                    BASIS_Mu	= obj.PHI * Mu_new;
                    %
                    % Compute outputs and error at new point
                    [dataError,y]	= obj.data_error(BASIS_Mu);
                    regulariser		= (obj.Alpha'*(Mu_new.^2))/2;
                    newTotalError	= dataError + regulariser;
                    %
                    % Test that we haven't made things worse
                    if newTotalError >= errorLog(iteration)
                        % If so, back off!
                        step	= step/2;
                    else
                        obj.Mu	= Mu_new;
                        step	= 0;			% this will force exit from the "while" loop
                    end
                end
            end
            %
            % Simple computation of return value of log likelihood at mode
            likelihoodMode	= -dataError;
        end
        %
        function [e,y] = data_error(obj, BASIS_Mu)
            y	= obj.sigmoid(BASIS_Mu);
%             y	= 1 ./ (1+exp(-BASIS_Mu)); % sigmoid(BASIS_Mu);
            % Handle probability zero cases
            y0	= (y==0);
            y1	= (y==1);
            if any(y0(obj.Targets > 0)) || any(y1(obj.Targets < 1))
                % Error infinite when model gives zero probability in
                % contradiction to data
                e	= inf;
            else
                % Any y=0 or y=1 cases must now be accompanied by appropriate
                % output=0 or output=1 values, so can be excluded.
                e	= -(obj.Targets(~y0)' * log(y(~y0)) ...
                    + (1 - obj.Targets(~y1))' * log(1-y(~y1)));
            end
        end
        %
        function logML = compute_statistics(obj)
            % Posterior must be approximated: find the posterior mode as the basis
            % for the Laplace approximation
            [dataLikely, badHess] = obj.posterior_mode();
            
            % Compute covariance approximation
            Ui	= obj.U \ eye(size(obj.U)); % inv(U)
            obj.SIGMA	= Ui * Ui';
            % Compute posterior-mean-based outputs and errors
            y	= obj.sigmoid(obj.PHI * obj.Mu);
%             y   = 1 ./ (1+exp(-obj.PHI * obj.Mu));
            e		= (obj.Targets - y);
            
            % COMPUTE LOG MARGINAL LIKELIHOOD
            logdetHOver2	= sum(log(diag(obj.U)));
            logML		= dataLikely - (obj.Mu.^2)' * obj.Alpha/2 + ...
                sum(log(obj.Alpha))/2 - logdetHOver2;
            
            % Well-determinedness factors
            %             DiagC	= sum(Ui.^2,2);
            obj.Gamma	= 1 - obj.Alpha .* sum(Ui.^2,2);
            
            % COMPUTE THE Q & S VALUES
            % Q: "quality" factor - related to how well the basis function contributes
            % to reducing the error
            %
            % S: "sparsity factor" - related to how orthogonal a given basis function
            % is to the currently used set of basis functions
            
            % Non-Gaussian case: beta an N-vector
            obj.BASIS_B_PHI	= obj.BASIS' * (obj.PHI .* (obj.beta * ones(1,obj.M)));
            % The S_in calculation exploits the fact that BASIS is normalised,
            %  i.e. sum(BASIS.^2,1)==ones(1,M_full)
            obj.S_in		= (obj.beta' * (obj.BASIS.^2))' - sum((obj.BASIS_B_PHI * Ui).^2,2);
            obj.Q_in		= obj.BASIS' * e;
            obj.S_out		= obj.S_in;
            obj.Q_out		= obj.Q_in;
            
            % S,Q with that basis excluded: equations (23)
            obj.S_out(obj.Used)	= (obj.Alpha .* obj.S_in(obj.Used)) ./ (obj.Alpha - obj.S_in(obj.Used));
            obj.Q_out(obj.Used)	= (obj.Alpha .* obj.Q_in(obj.Used)) ./ (obj.Alpha - obj.S_in(obj.Used));
            % Pre-compute the "relevance factor" for ongoing convenience
            obj.Factor		= (obj.Q_out .* obj.Q_out - obj.S_out);
        end
        %
        function [DeltaML, Action] = decision_phase(obj)
            % DECISION PHASE
            % Assess all potential actions
            % Compute change in likelihood for all possible updates
            DeltaML		= zeros(obj.M_full,1);
            Action		= obj.ACTION_REESTIMATE * ones(obj.M_full,1); % Default
            %
            % 'Relevance Factor' (Q^S-S) values for basis functions in model
            UsedFactor	= obj.Factor(obj.Used);
            
            % RE-ESTIMATION: must be a POSITIVE 'factor' and already IN the model
            iu		= UsedFactor > obj.ZeroFactor;
            index		= obj.Used(iu);
            NewAlpha	= obj.S_out(index).^2 ./ obj.Factor(index);
            Delta		= (1./NewAlpha - 1./obj.Alpha(iu)); % Temp vector
            
            % Quick computation of change in log-likelihood given all re-estimations
            DeltaML(index)	= (Delta .* (obj.Q_in(index).^2) ./ ...
                (Delta .* obj.S_in(index) + 1) - ...
                log(1 + obj.S_in(index).*Delta))/2;
            
            % DELETION: if NEGATIVE factor and IN model
            % But don't delete:
            %		- any "free" basis functions (e.g. the "bias")
            %		- if there is only one basis function (M=1)
            %
            % (In practice, this latter event ought only to happen with the Gaussian
            % likelihood when initial noise is too high. In that case, a later beta
            % update should 'cure' this.)
            iu			= ~iu; 	% iu = UsedFactor <= CONTROLS.ZeroFactor
            index			= obj.Used(iu);
            obj.anyToDelete	= ~isempty(setdiff(index,[])) && obj.M > 1;
            %
            if obj.anyToDelete
                % Quick computation of change in log-likelihood given all deletions
                DeltaML(index)	= -(obj.Q_out(index).^2 ./ (obj.S_out(index) + obj.Alpha(iu)) - ...
                    log(1 + obj.S_out(index) ./ obj.Alpha(iu)))/2;
                Action(index)	= obj.ACTION_DELETE;
                % Note: if M==1, DeltaML will be left as zero, which is fine
            end
            
            % ADDITION: must be a POSITIVE factor and OUT of the model
            % Find ALL good factors ...
            GoodFactor	= obj.Factor > obj.ZeroFactor;
            % ... then mask out those already in model
            GoodFactor(obj.Used) = 0;
            % ... and then mask out any that are aligned with those in the model
            if obj.BasisAlignmentTest
                GoodFactor(obj.Aligned_out)	= 0;
            end
            %
            index = find(GoodFactor);
            obj.anyToAdd = ~isempty(index);
            if obj.anyToAdd
                %
                % Quick computation of change in log-likelihood given all additions
                %
                quot			= obj.Q_in(index).^2 ./ obj.S_in(index);
                DeltaML(index)	= (quot - 1 - log(quot))/2;
                Action(index)	= obj.ACTION_ADD;
            end
        end
        %
        function j = postprocess_action(obj, DeltaML, Action)
            % Post-process action results to take account of preferences
            % If we prefer ADD or DELETE actions over RE-ESTIMATION
            if (obj.anyToAdd && obj.PriorityAddition) || ...
                    (obj.anyToDelete && obj.PriorityDeletion)
                % We won't perform re-estimation this iteration, which we achieve by
                % zero-ing out the delta
                DeltaML(Action == obj.ACTION_REESTIMATE)	= 0;
                % Furthermore, we should enforce ADD if preferred and DELETE is not
                % - and vice-versa
                if (obj.anyToAdd && obj.PriorityAddition && ~obj.PriorityDeletion)
                    DeltaML(Action == obj.ACTION_DELETE)	= 0;
                end
                if (obj.anyToDelete && obj.PriorityDeletion && ~obj.PriorityAddition)
                    DeltaML(Action == obj.ACTION_ADD)		= 0;
                end
            end
            
            % Finally...we choose the action that results
            % in the greatest change in likelihood
            %
            [obj.deltaLogMarginal, obj.nu]	= max(DeltaML);
            obj.selectedAction		= Action(obj.nu);
            obj.anyWorthwhileAction	= obj.deltaLogMarginal > 0;
            %
            % We need to note if basis obj.nu is already in the model, and if so,
            % find its interior index, denoted by "j"
            j = [];
            if obj.selectedAction == obj.ACTION_REESTIMATE || obj.selectedAction == obj.ACTION_DELETE
                j		= find(obj.Used==obj.nu);
            end
            %
            % Get the individual basis vector for update and compute its optimal alpha,
            % according to equation (20): alpha = S_out^2 / (Q_out^2 - S_out)
            %
            obj.Phi		= obj.BASIS(:,obj.nu);
            obj.newAlpha	= obj.S_out(obj.nu)^2 / obj.Factor(obj.nu);
        end
        %
        function alignment_check(obj)
            % ALIGNMENT CHECKS
            % If we're checking "alignment", we may have further processing to do
            % on addition and deletion
            if obj.BasisAlignmentTest
                % Addition - rule out addition (from now onwards) if the new basis
                % vector is aligned too closely to one or more already in the model
                if obj.selectedAction == obj.ACTION_ADD
                    % Basic test for correlated basis vectors
                    % (note, Phi and columns of PHI are normalised)
                    p				= obj.Phi' * obj.PHI;
                    findAligned	= find(p > obj.AlignmentMax);
                    numAligned	= length(findAligned);
                    if numAligned > 0
                        % The added basis function is effectively indistinguishable from
                        % one present already
                        obj.selectedAction	= obj.ACTION_ALIGNMENT_SKIP;
                        obj.alignDeferCount	= obj.alignDeferCount + 1;
                        % Make a note so we don't try this next time
                        % May be more than one in the model, which we need to note was
                        % the cause of function 'obj.nu' being rejected
                        obj.Aligned_out	= [obj.Aligned_out ; obj.nu * ones(numAligned,1)];
                        obj.Aligned_in	= [obj.Aligned_in ; obj.Used(findAligned)];
                    end
                end
                %
                % Deletion: reinstate any previously deferred basis functions
                % resulting from this basis function
                %
                if obj.selectedAction == obj.ACTION_DELETE
                    findAligned	= find(obj.Aligned_in == obj.nu);
                    numAligned	= length(findAligned);
                    if numAligned > 0
                        obj.Aligned_in(findAligned)		= [];
                        obj.Aligned_out(findAligned)	= [];
                    end
                end
            end
        end
        %
        function action_phase(obj, j)
            % We'll want to note if we've made a change which necessitates later
            % updating of the statistics
            obj.UPDATE_REQUIRED	= false;
            %
            switch obj.selectedAction
                case obj.ACTION_REESTIMATE
                    %
                    % Basis function 'obj.nu' is already in the model,
                    % and we're re-estimating its corresponding alpha
                    % - should correspond to Appendix A.3
                    %
                    oldAlpha	= obj.Alpha(j);
                    obj.Alpha(j)	= obj.newAlpha;
                    s_j			= obj.SIGMA(:,j);
                    deltaInv	= 1/(obj.newAlpha - oldAlpha);
                    kappa		= 1/(obj.SIGMA(j,j)+deltaInv);
                    tmp			= kappa*s_j;
                    obj.SIGMANEW	= obj.SIGMA - tmp * s_j';
                    deltaMu		= -obj.Mu(j) * tmp;
                    obj.Mu			= obj.Mu + deltaMu;
                    obj.UPDATE_REQUIRED	= true;
                    %
                case obj.ACTION_ADD
                    % Basis function obj.nu is not in the model, and we're adding it in
                    % - should correspond to Appendix A.2
                    B_Phi	= obj.Phi .* obj.beta;
                    tmp		= ((B_Phi' * obj.PHI) * obj.SIGMA)';
                    %
                    obj.Alpha	= [obj.Alpha ; obj.newAlpha];
                    obj.PHI		= [obj.PHI obj.Phi];
                    %
                    s_ii		= 1/(obj.newAlpha + obj.S_in(obj.nu));
                    s_i			= -s_ii*tmp;
                    TAU			= -s_i*tmp';
                    obj.SIGMANEW	= [obj.SIGMA + TAU s_i ; s_i' s_ii];
                    mu_i		= s_ii * obj.Q_in(obj.nu);
                    deltaMu		= [-mu_i*tmp ; mu_i];
                    obj.Mu			= [obj.Mu ; 0] + deltaMu;
                    obj.Used		= [obj.Used; obj.nu];
                    obj.UPDATE_REQUIRED	= true;
                    
                case obj.ACTION_DELETE
                    % Basis function obj.nu is in the model, but we're removing it
                    % - should correspond to Appendix A.4
                    obj.PHI(:,j)	= [];
                    obj.Alpha(j)	= [];
                    %
                    s_jj			= obj.SIGMA(j,j);
                    s_j				= obj.SIGMA(:,j);
                    tmp				= s_j/s_jj;
                    obj.SIGMANEW		= obj.SIGMA - tmp*s_j';
                    obj.SIGMANEW(j,:)	= [];
                    obj.SIGMANEW(:,j)	= [];
                    deltaMu			= - obj.Mu(j)*tmp;
                    obj.Mu				= obj.Mu +deltaMu;
                    obj.Mu(j)			= [];
                    obj.Used(j)		= [];
                    obj.UPDATE_REQUIRED	= true;
            end					% of switch over actions
            obj.M		= length(obj.Used);
        end
        %
        function update_statistics(obj)
            % UPDATE STATISTICS
            % If we've performed a meaningful action,
            % update the relevant variables
            if obj.UPDATE_REQUIRED
                % Compute all statistics in "full" form (non-Gaussian likelihoods)
                newLogM = obj.compute_statistics();
                obj.deltaLogMarginal	= newLogM - obj.logML;
                obj.logML               = newLogM;
            end
        end
        %
        function run(obj)
            i				= 0;	% Iteration number
            LAST_ITERATION	= false;
            %
            while (~LAST_ITERATION)
                %
                i	= i+1;
                %                     obj.UpdateIteration = 0;
                
                % Decide what to do
                [DeltaML, Action] = obj.decision_phase();
                
                % Post-process action results to take account of preferences
                j = obj.postprocess_action(DeltaML, Action);
                
                % TERMINATION CONDITIONS
                % Propose to terminate if:
                % 1.	there is no worthwhile (likelihood-increasing) action, OR
                % 2a.	the best action is an ACTION_REESTIMATE but this would only lead to
                %		an infinitesimal alpha change, AND
                % 2b.	at the same time there are no potential awaiting deletions
                if ~obj.anyWorthwhileAction || ...
                        (obj.selectedAction == obj.ACTION_REESTIMATE && ...
                        abs(log(obj.newAlpha) - log(obj.Alpha(j)))<obj.MinDeltaLogAlpha && ...
                        ~obj.anyToDelete)
                    %
                    obj.selectedAction	= obj.ACTION_TERMINATE;
                end
                
                % alignment check
                obj.alignment_check();
                
                % Implement above decision
                obj.action_phase(j);
                
                % update variables
                obj.update_statistics();
                
                % Check for "natural" termination
                ITERATION_LIMIT	= (i == obj.iterations);
                LAST_ITERATION	= ITERATION_LIMIT;
            end
            
            % OUTPUT VARIABLES
            % We also choose to sort here - it can't hurt and may help later
            [obj.PARAMETER.Relevant, index]	= sort(obj.Used);
            
            % Not forgetting to correct for normalisation too
            obj.PARAMETER.Value	= obj.Mu(index) ./ obj.BasisScales(obj.Used(index))';
            
            obj.HYPERPARAMETER.Alpha	= obj.Alpha(index)./(obj.BasisScales(obj.Used(index))'.^2);
            obj.HYPERPARAMETER.beta		= obj.beta;
            
            obj.DIAGNOSTIC.Gamma		= obj.Gamma(index);
            obj.DIAGNOSTIC.iterations	= i;
            obj.DIAGNOSTIC.S_Factor     = obj.S_out;
            obj.DIAGNOSTIC.Q_Factor     = obj.Q_out;
            obj.DIAGNOSTIC.M_full		= obj.M_full;
        end
    end
end