%% Evaluate
% get gt label
gtLabelAll = [];
points3d_gt_all = [];

gt_name = ['Cam2_' frame];
T = readtable(gt_name);
points3d_gt = table2array(T);
gtLabel = points3d_gt(:, end);  % labels
points3d_gt_all = [points3d_gt_all; points3d_gt];
gtLabelAll = [gtLabelAll; gtLabel];

gt_name = ['Cam3_' frame];
T = readtable(gt_name);
points3d_gt = table2array(T);
gtLabel = points3d_gt(:, end);  % labels
points3d_gt_all = [points3d_gt_all; points3d_gt];
gtLabelAll = [gtLabelAll; gtLabel];

gt_name = ['Cam5_' frame];
T = readtable(gt_name);
points3d_gt = table2array(T);
gtLabel = points3d_gt(:, end);  % labels
points3d_gt_all = [points3d_gt_all; points3d_gt];
gtLabelAll = [gtLabelAll; gtLabel];

roc_prior  = multiClassROCanalysis(points3d_gt_all(gtLabelAll~=0, 18:end-1), gtLabelAll(gtLabelAll~=0) + 1);
roc_points = multiClassROCanalysis(points3d_gt_all(gtLabelAll~=0, 4:17),   gtLabelAll(gtLabelAll~=0) + 1);

%% Plot results
% % predicted map
% outcmap = NCLTcolormap(Yid_points);
% outmap  = pointCloud(t_points(:, 1:3), 'Color', outcmap);
% % original map
% incmap  = NCLTcolormap(L_valid + 1);
% inmap   = pointCloud(t_points(:, 1:3), 'Color', incmap);
% % gt map
% gtcmap  = NCLTcolormap(gtLabelAll + 1);
% gtmap   = pointCloud(points3d_gt_all(:, 1:3) + 1, 'Color', gtcmap);
% 
% % plot RVM output
% figure; 
% subplot(3,1,1); pcshow(outmap), axis equal tight, axis off, title('rvm points')
% subplot(3,1,2); pcshow(inmap), axis equal tight, axis off, title('prior')
% subplot(3,1,3); pcshow(gtmap), axis equal tight, axis off, title('ground truth')

% print evaluation result
method = {'Prior'; 'RVM-points'};
Recall = [roc_prior.sensitivityTotal;roc_points.sensitivityTotal];
Accuracy = [roc_prior.accuracyTotal;roc_points.accuracyTotal];
Precision = [roc_prior.precisionTotal;roc_points.precisionTotal];
IoU = [roc_prior.IoUtot;roc_points.IoUtot];
AUC = [roc_prior.AUCtot;roc_points.AUCtot];

T = table(method, Recall, Accuracy, Precision, IoU, AUC);
disp(T)