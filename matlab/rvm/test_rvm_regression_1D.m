clc; clear; close all
% test for rvm regression class. 1D 
% generate some data
N           = 100;	% Number of points
basisWidth  = 0.05;		% NB: data is in [0,1]
iterations	= 500;
X           = [0:N-1]'/N;

% Compute squared exponential basis (design) matrix
BASIS       = exp(-distSquared(X,X) / (basisWidth^2));

% Randomise some weights, then make each weight sparse with probability
% pSparse 
pSparse		= 0.90;
M			= size(BASIS,2);
w			= randn(M,1) * 100 / (M * (1 - pSparse));
sparse		= rand(M,1) < pSparse;
w(sparse)	= 0;

% Now we have the basis and weights, compute linear model
z			= BASIS*w;

% Generate our data by adding some noise on to the generative function
noiseToSignal	= 0.2;
noise		= std(z) * noiseToSignal;
Targets     = z + noise * randn(N,1);

%% RVM
reg = rvm_regression(X, Targets);
reg.run;

%% OUTPUT VARIABLES
PARAMETER			= reg.PARAMETER;
HYPERPARAMETER		= reg.HYPERPARAMETER;

% Manipulate the returned weights for convenience later
w_infer						= zeros(size(BASIS,2),1);
w_infer(PARAMETER.Relevant)	= PARAMETER.Value;

% Compute squared exponential basis (design) matrix
BASIS       = exp(-distSquared(X,X) / (basisWidth^2));
% Compute the inferred prediction function
y_l	= BASIS * w_infer;


%% Plot the result
figure;
clf
whitebg(1,'k')
subplot(1,3,1)
plot(X,z,'w-','linewidth',2);
hold on
plot(X,y_l,'r-','linewidth',2);
hold off

% Compare the data and the predictive model (post link-function)
subplot(1,3,2)
plot(X,Targets,'w.','linewidth',4);
hold on
plot(X,y_l,'r-','linewidth',3);
plot(X(PARAMETER.Relevant),Targets(PARAMETER.Relevant),'yo')
hold off

% Show the inferred weights
subplot(1,3,3)
h	= stem(w_infer,'filled');
set(h,'Markersize',3,'Color','r')
set(gca,'Xlim',[0 N+1])


