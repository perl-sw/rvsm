% Example for single scan RVM semantic map
% Lu Gan, ganlu@umiche.edu

%{
class_name       = [r, g, b, label];
1   building     = [128/255,    0,          0,       0];
2   sky          = [128/255,    128/255,    128/255, 1];
3   road         = [128/255,    64/255,     128/255, 2];
4   vegetation   = [128/255,    128/255,    0,       3];
5   sidewalk     = [0,          0,          192/255, 4];
6   car          = [64/255,     0,          128/255, 5];
7   pedestrain   = [64/128,     64/128,     0,       6];
8   cyclist      = [0,          128/255,    192/255, 7];
9   signate      = [192/255,    128/255,    128/255, 8];
10  fence        = [64/255,     64/255,     128/255, 9];
11  pole         = [192/255,    192/255,    128/255, 10];
%}

clc; clear; close all

% load labeled point cloud as prior
frame = '1335704347924201.txt';
T = readtable(frame);
points3d = table2array(T);
L = points3d(:, end);  % labels

% find valid points
invalid_label = 0;  % background label
L_valid = L(L~=invalid_label);
points3d_valid = points3d(L~=invalid_label,:);
% L_valid = L;
% points3d_valid = points3d;

% invalid_label = 13;  % sky label
% L_valid = L_valid(L_valid~=invalid_label);
% points3d_valid = points3d_valid(L_valid~=invalid_label,:);

% invalid_label = 8;  % sky label
% L_valid = L_valid(L_valid~=invalid_label);
% points3d_valid = points3d_valid(L_valid~=invalid_label,:);


% number of classes
nc = 14;
 
% method 1: make sure there are enough points from each class kept
r = 0.04; % keep r percent of data
idx_downsampled= [];

lid = unique(L_valid);
for i = 1:length(lid)
    idx = find(L == lid(i));
    reduced_size = min(max(floor(r * length(idx)), min(20, length(idx))), 1000);
    idx_reduced = floor(rand(reduced_size,1) * length(idx)) + 1;
    idx_downsampled = [idx_downsampled; idx(idx_reduced)];
end
X = points3d_valid(idx_downsampled, 1:3);  % only XYZ
y = L_valid(idx_downsampled);



% downsample the pointcloud
% skip = 10;
% X = points3d_valid(1:skip:end, 1:3); 
% X = (X - mean(X));

% initialise target
% y = L_valid(1:skip:end);
yl = cell(nc,1);

%% RVM Semantic Map
% Heuristically adjust basis width to account for distance scaling with dimension.
% dimension = size(X,2);
% basisWidth = 0.05;
% basisWidth	= basisWidth^(1 / dimension);		% NB: data is in [0,1]
% % Compute squared basis (design) matrix
% BASIS            = 0.1^2 * exp(-distSquared(X,X) / (basisWidth^2));

% covfunc = {@covSEiso}; hyp = []; hyp.cov = log([6 4.8]);
% covfunc = {@covSEard}; hyp = []; hyp.cov = log([1.8096 1.6246 1.9090 2.8987]);
% covfunc = {@covMaterniso, 5}; hyp = []; hyp.cov = log([6 4.8]);
covfunc = {@covMaternard, 5}; hyp = []; hyp.cov = log([8 4.8 6 4.8]);
BASIS = feval(covfunc{:}, hyp.cov, X);

% Train and Test
t_points = points3d_valid(:, 1:3);  % query for point clouds
% t_points = (t_points - mean(t_points));

PARAMETER = cell(nc,1);
relevance_vectors = [];
relevance_vector_labels = [];
t0 = tic;
for i = 1:nc
    positiveLabels = y == i - 1;  % labels: 0-10
    yl{i} = double(positiveLabels); 
    disp(['class ', num2str(i), ': ', num2str(sum(yl{i}))])
    
    cla = rvm_classification(X, yl{i}, BASIS);
    cla.run;
     
    % OUTPUT VARIABLES
    PARAMETER{i}			= cla.PARAMETER;
    HYPERPARAMETER{i}		= cla.HYPERPARAMETER;
    
    relevance_vector{i} = X(PARAMETER{i}.Relevant, :);
    relevance_vectors = [relevance_vectors; relevance_vector{i}];
    relevance_vector_labels = [relevance_vector_labels; (i-1)*ones(size(relevance_vector{i}, 1),1)];
    
    w_infer{i}	        = PARAMETER{i}.Value;
    test_BASIS_points         = feval(covfunc{:}, hyp.cov, relevance_vector{i}, t_points)'; 
    
    % Compute the inferred prediction function
    y_out_points{i} = test_BASIS_points * w_infer{i};
%     y_out_points{i} = 1./(1+exp(-y_out_points{i}));
    
end
time = toc(t0);

[Y_points, Ymax_points, Yid_points] = softHardLabels(y_out_points);  % Yid is predicted labels



t_points_labels = 0*ones(size(X, 1),1);
relevance_vectors = [X; relevance_vectors  ];
relevance_vector_labels = [t_points_labels ; relevance_vector_labels ];


%% Plot results
% predicted map
rvcmap = NCLTcolormap(relevance_vector_labels + 1);
rvmap  = pointCloud(relevance_vectors, 'Color', rvcmap);
% % original map
incmap  = NCLTcolormap(L_valid + 1);
inmap   = pointCloud(t_points(:, 1:3), 'Color', incmap);
% % gt map
% gtcmap  = NCLTcolormap(gtLabelAll + 1);
% gtmap   = pointCloud(points3d_gt_all(:, 1:3) + 1, 'Color', gtcmap);
outcmap = NCLTcolormap(Yid_points);
outmap = pointCloud(points3d_valid(:, 1:3), 'Color', outcmap);


% plot RVM output
figure; 
pcshow(outmap, 'MarkerSize', 100), axis equal tight, axis off
