# bayesian-inference

Bayesian Inference methods cc/python implementation

---

# Requirements

* Bazel: [https://github.com/bazelbuild/bazel/releases](https://github.com/bazelbuild/bazel/releases)
* Ceres: [https://github.com/ceres-solver/ceres-solver](https://github.com/ceres-solver/ceres-solver)
* Glog: [https://github.com/google/glog](https://github.com/google/glog)

---

# Install

```
git clone git@bitbucket.org:perl-sw/bayesian-inference.git
cd bayesian-inference/cc
bazel build //exec:rvm-exp
```