#pragma once

#include <cmath>
#include "rvm/rvm.h"

namespace library {
namespace bayesian_inference {

#define BETA_MIN 1e-2

template <class T>
class RvmClassification: public Rvm<T> {
 public:
  // data is n_samples X dimension
  // labels is n_samples X 1
  RvmClassification(const std::vector<T> &data, const std::vector<int> &labels, const IKernel<T> *kernel) :
   kernel_(kernel),
   eps(0.01),
   training_data_(data),
   training_labels_(Eigen::VectorXd::Zero(labels.size())),
   x_m_(training_data_),
   w_(Eigen::MatrixXd::Zero(data.size(), 1)),
   alpha_(Eigen::MatrixXd::Ones(data.size(), 1)) {
    BOOST_ASSERT(data.size() == labels.size());
    //std::cout << phi_samples_ << std::endl;   
    for (size_t i=0; i<labels.size(); i++) {
      training_labels_(i) = labels[i];
    }
  }

  const std::vector<T>& GetRelevanceVectors() const {
    return x_m_;
  }

  const Eigen::MatrixXd& GetWeights() const {
    return w_;
  }

  virtual size_t NumRelevanceVectors() const {
    return GetRelevanceVectors().size();
  }

  double ComputeLogLikelihood(const Eigen::MatrixXd &w, Eigen::MatrixXd *gradient = nullptr) const {
    return 0;
  }

  void Solve(const int max_iterations) {
    // Make a target vector where +1 examples have value 1 and -1 examples
    // have a value of 0.
    
    /*! This is the convention for the active_bases variable in the function:
        - if (active_bases(i) >= 0) then
            - alpha(active_bases(i)) == the alpha value associated with sample x(i)
            - weights(active_bases(i)) == the weight value associated with sample x(i)
            - colm(phi, active_bases(i)) == the column of phi associated with sample x(i)
            - colm(phi, active_bases(i)) == kernel column i (from get_kernel_colum()) 
        - else
            - the i'th sample isn't in the model and notionally has an alpha of infinity and
              a weight of 0.
    !*/

    // Set the initial values of these guys
    std::vector<long> active_bases(training_data_.size());
    std::fill(active_bases.begin(), active_bases.end(), -1);

    // Initialize with a single basis vector and set its alpha
    x_m_.clear();
    long first_basis = PickVector();
    x_m_.push_back(training_data_[first_basis]);
    active_bases[first_basis] = 0;
    std::cout << "Computing Phi " << first_basis << "\n";
    phi_samples_ = GetKernelColumn(first_basis);
    
    alpha_.resize(1,1);
    //alpha_(0,0) = phi_samples_.squaredNorm()/((phi_samples_.transpose()*training_labels_).squaredNorm()/phi_samples_.squaredNorm()-var); // TODO: initialize alpha without variance
    alpha_(0,0) = 1;

    w_.resize(1,1);
    w_(0,0) = 0;
    //sigma_.resize(1,1);
    
    Eigen::MatrixXd prev_alpha = alpha_;
    Eigen::MatrixXd prev_w = w_;

    // Now declare a bunch of other variables we will be using below
    Eigen::MatrixXd Q(training_data_.size(), 1);
    Eigen::MatrixXd S(training_data_.size(), 1);
 
    bool recompute_beta = true;
    bool search_all_alphas = false;
    unsigned long ticker = 0;
    unsigned long iteration = 0;
    const unsigned long rounds_of_narrow_search = 100;

    while (true) {

      iteration++;
      std::cout << "Iteration: " << iteration << std::endl;
      printf("LL: %5.3f Number Relevance Vectors: %d\n", ComputeLogLikelihood(GetWeights()), NumRelevanceVectors());
      if (iteration>max_iterations)
        break;

      // Compute the current beta
      if (recompute_beta) {
        UpdateBeta();
        recompute_beta = false;
      }
      
      double data_likely = PosteriorMode();
      Eigen::VectorXd error = training_labels_ - y_;
      t_hat_ = t_estimate_;
      for (long r = 0; r < t_estimate_.rows(); ++r)  
        t_hat_(r) += 1/beta_(r) * error(r);

      // Check if we should do a full search for the best alpha to optimize
      if (ticker == rounds_of_narrow_search) {
        
        // if the current alpha and weights are equal to what they were
        // at the last time we were about to start a wide search then
        // we are done.
        if (Equal(prev_alpha, alpha_) && Equal(prev_w, w_)) {
          std::cout << "Break due to No alpha change\n";
          break;
        }

        prev_alpha = alpha_;
        prev_w = w_;
        search_all_alphas = true;
        ticker = 0;
      } else {
        search_all_alphas = false;
      }
      ++ticker;

      // Compute S and Q using equations 24 and 25 (tempv = phi*sigma*trans(phi)*B*t_hat)
      for (long i = 0; i < S.rows(); ++i) {
        // If we are currently limiting the search for the next alpha to update
        // to the set in the active set then skip a non-active vector.
        if ( active_bases[i] == -1)
          continue;

        // Get the column for this sample out of the kernel matrix.  If it is
        // something in the active set then just get it right out of phi, otherwise
        // we have to calculate it.
        Eigen::SparseMatrix<double> K_col;
        if (active_bases[i] != -1) {
          K_col = phi_samples_.col(active_bases[i]);
        } else {
          K_col = GetKernelColumn(i);
        }

        Eigen::MatrixXd tempv1 = K_col.transpose() * beta_.asDiagonal() * phi_samples_;
        Eigen::MatrixXd tempv2 = beta_.asDiagonal() * t_hat_;
        S.block(i, 0, 1, 1) = K_col.transpose() * beta_.asDiagonal() * K_col - tempv1 * sigma_ * tempv1.transpose();
        Q.block(i, 0, 1, 1) = K_col.transpose() * tempv2 - tempv1 * sigma_ * phi_samples_.transpose() * tempv2;
      }

      std::cout << "search_all_alphas: " << search_all_alphas << std::endl;
      
      const long selected_idx = NextBestAlpha(S, Q, active_bases, search_all_alphas);
      std::cout << "Computed Next best alpha " << selected_idx << " active basis " << active_bases[selected_idx] << "\n";

      // If find_next_best_alpha_to_update didn't find any good alpha to update
      if (selected_idx == -1) {
        if (search_all_alphas == false) {
          // Jump us to where search_all_alphas will be set to true and try again
          ticker = rounds_of_narrow_search;
          continue;
        } else {
          // We are really done so quit the main loop
          break;
        }
      }

      // Next we update the selected alpha.

      // If the selected alpha is in the active set
      if (active_bases[selected_idx] >= 0) {
        const long idx = active_bases[selected_idx];
        const double s = alpha_(idx, 0) * S(selected_idx, 0) / (alpha_(idx, 0) - S(selected_idx, 0));
        const double q = alpha_(idx, 0) * Q(selected_idx, 0) / (alpha_(idx, 0) - S(selected_idx, 0));

        double old = alpha_(idx, 0);
        if (q*q-s > 1e-12) {
          // Re-estimate the value of alpha
          alpha_(idx, 0) = s*s/(q*q-s);
          std::cout << "Restimate the Value of Alpha, old: " << old << " new " << alpha_(idx,0) << std::endl; 
        } else {
          std::cout << "Remove selected Alpha Value From model, value was " << 1.0/old << "\n";
          // The new alpha value is infinite so remove the selected alpha from our model
          active_bases[selected_idx] = -1;
          RemoveColumn(&phi_samples_, idx);
          RemoveRow(&w_, idx);
          RemoveRow(&alpha_, idx);
          x_m_.erase(x_m_.begin() + idx);

          // Fix the index values in active_bases
          for (long i = 0; i < active_bases.size(); ++i) {
            if (active_bases[i] > idx) {
              active_bases[i] -= 1;
            }
          }

          // We changed the number of weights so we need to remember to
          // recompute the beta vector next time around the main loop.
          recompute_beta = true;
        }
      } else {

        Eigen::SparseMatrix<double> K_col = GetKernelColumn(selected_idx);
        Eigen::MatrixXd tempv1 = K_col.transpose() * beta_.asDiagonal() * phi_samples_;
        Eigen::MatrixXd tempv2 = beta_.asDiagonal() * t_hat_;
        S.block(selected_idx, 0, 1, 1) = K_col.transpose() * beta_.asDiagonal() * K_col - tempv1 * sigma_ * tempv1.transpose();
        Q.block(selected_idx, 0, 1, 1) = K_col.transpose() * tempv2 - tempv1 * sigma_ * phi_samples_.transpose() * tempv2;
 
        // The selected alpha is not in the active set
        const double s = S(selected_idx, 0);
        const double q = Q(selected_idx, 0);

        if (q*q-s > 1e-12) {
          
          // Add the selected alpha to our model
          std::cout << "Add the selected alpha to our model\n";
          active_bases[selected_idx] = phi_samples_.cols();
          x_m_.push_back(training_data_[selected_idx]);

          // Update alpha
          Eigen::MatrixXd temp_alpha(alpha_.rows()+1, 1);
          temp_alpha.topLeftCorner(alpha_.rows(), alpha_.cols()) = alpha_;
          temp_alpha(alpha_.rows(), 0) = s*s/(q*q-s);
          alpha_ = temp_alpha;
 
          // Update weights
          Eigen::MatrixXd temp_w(w_.rows()+1, 1);
          temp_w.topLeftCorner(w_.rows(), w_.cols()) = w_;
          temp_w(w_.rows(), 0) = 0;
          w_ = temp_w;

          
          // Update phi by adding the new sample's kernel matrix column in as one of phi's columns
          Eigen::SparseMatrix<double> temp_phi;
          temp_phi.conservativeResize(phi_samples_.rows(), phi_samples_.cols()+1);
          temp_phi.leftCols(phi_samples_.cols()) = phi_samples_;
          temp_phi.col(phi_samples_.cols()) = GetKernelColumn(selected_idx);
          phi_samples_ = temp_phi;
          
          // we changed the number of weights so we need to remember to 
          // recompute the beta vector next time around the main loop.
          recompute_beta = true;
        }
      }
    } // end while(true).  So we have converged on the final answer.
    
    std::cout << "Phi Size: " << phi_samples_.cols() << " w size: " << w_.rows() << std::endl;
    std::cout << "w_ " << w_ << std::endl;
  }


  class RvmClassificationDecisionFunction {
  public:
    RvmClassificationDecisionFunction( const std::vector<T> x_m, const Eigen::MatrixXd w, const IKernel<T> *kernel) :
      x_m_(x_m),
      w_(w),
      kernel_(kernel) {};
      
    double PredictLabel(const T& sample) const {
      //std::cout << "Predict Label\n " << sample << "\n";
      Eigen::MatrixXd phi = ComputePhi(sample);
      //std::cout << "X\n " << x_m_[0] << "\nw\n" << w_  << "\nPhi\n" << phi << "\n";
      
      double y = (phi *w_)(0,0);
      double val = 1 / (1 + std::exp(-y));
      // std::cout << "val " << val << std::endl;
      return val;
    }

    /*std::vector<double> PredictLabels(const std::vector<T> &samples) const {
      Eigen::MatrixXd phi = ComputePhi(samples);

      Eigen::MatrixXd exponent = -phi * w_;
      auto exp = exponent.array().exp();  // Why not negative ???
      auto prob = (1 + exp).inverse();

      // map to std::vector
      std::vector<double> res;
      res.resize(samples.size());
      Eigen::VectorXd::Map(&res[0], samples.size()) = prob;

      return res;
    }*/

  private:
    const std::vector<T> x_m_;
    const Eigen::MatrixXd w_;
    const IKernel<T> *kernel_;

    /*Eigen::SparseMatrix<double> ComputePhi(const std::vector<T> &data) const {
      //Eigen::MatrixXd phi(data.rows(), x_m_.rows());
      Eigen::SparseMatrix<double> phi;
      phi.conservativeResize(data.size(), x_m_.size());

      for (size_t i=0; i<data.size(); i++) {
        T sample = data[i];
        for (size_t j=0; j<x_m_.size(); j++){
          T x_m = x_m_[j];

          // Sparsify
          double val = kernel_->Compute(sample, x_m);
          if (std::abs(val) > 1e-5) {
            phi.insert(i, j) = val;
          }
        }
      }

      return phi;
    }*/

      
    Eigen::MatrixXd ComputePhi(const T &data) const {
      //Eigen::MatrixXd phi(data.rows(), x_m_.rows());
      Eigen::MatrixXd phi(1, x_m_.size());
      for (size_t j=0; j<x_m_.size(); j++){
        const T& x_m = x_m_[j];
        //std::cout << "data\n" << data << "\n x_m\n" << x_m << "\nsq nm\n" << (data-x_m).squaredNorm() << "\n exp\n" << std::exp(-(data-x_m).squaredNorm()/(5.5*5.5)) << std::endl;
        //std::cout << "Before Compute\n";
        phi(0,j) = kernel_->Compute(data, x_m);
        //std::cout << "phi\n" << phi(0,j) << std::endl;
      }
      return phi;
    }
};

  std::unique_ptr<RvmClassificationDecisionFunction> GetDecisionFunction() {
    return std::make_unique<RvmClassificationDecisionFunction>(x_m_, w_, kernel_);
  }



 private:
  const IKernel<T> *kernel_;

  const double eps;

  std::vector<T> training_data_;
  Eigen::VectorXd training_labels_;

  std::vector<T> x_m_;
  Eigen::MatrixXd w_;
  Eigen::MatrixXd alpha_;
  Eigen::MatrixXd sigma_;

  Eigen::VectorXd t_estimate_;
  Eigen::VectorXd y_;
  Eigen::VectorXd beta_;
  Eigen::VectorXd t_hat_;

  Eigen::SparseMatrix<double> phi_samples_;


  void UpdateBeta() {
    // Calculate the current t_estimate. (this is the predicted t value for each sample according to the
    // current state of the classifier)
    t_estimate_ = phi_samples_ * w_;
    y_ = Sigmoid(t_estimate_);
    beta_ = y_.array() * (1 - y_.array());
    
    // Check beta
    for (long r = 0; r < beta_.rows(); ++r) {
      if (beta_(r) < BETA_MIN)
        beta_(r) = BETA_MIN;
    }
  }

  void UpdateSigma() {
    sigma_ = phi_samples_.transpose() * beta_.asDiagonal() * phi_samples_;
    for (long r = 0; r < alpha_.rows(); ++r)
      sigma_(r,r) += alpha_(r);
    sigma_ = sigma_.inverse();
  }


  /* 
   * This function finds the mode of the posterior dist. over parameters.
   * The optimization uses a variable-step-length second-order gradient-based 
   * (Newton) method.
   */
  double PosteriorMode() {
    // Tolerances
    double gradient_min = 1e-6; // termination criterion for each gradient dimension
    double step_min = 1/(2^8);  // minimum fraction of the full Newton step considered
        
    /*Eigen::SparseMatrix<double> A;
    A.conservativeResize(alpha_.rows(), alpha_.rows());
    for (int i=0; i<alpha_.rows(); i++) {
      A.insert(i, i) = alpha_(i);
    }*/

    // NB: for historical reasons, we work in term of error here (negative
    // log-liklihood) and minimise
    
    // Get current model output and data error
    Eigen::VectorXd t_estimate = phi_samples_ * w_;
    Eigen::VectorXd y = Sigmoid(t_estimate);
            
    double data_error = DataError(y);

    // Add on the weight penalty
    double regularizer = (alpha_.array() * (w_.array().square())).sum() / 2;
    double new_total_error = data_error + regularizer;

    int its_max = 25;
    double prev_error = 0;

    for (int i = 0; i < its_max; ++i) {
      // Log the error value each iteration
      prev_error = new_total_error;

      // Construct the gradient
      Eigen::VectorXd e = training_labels_ - y;
      Eigen::VectorXd g = phi_samples_.transpose() * e - (alpha_.array() * w_.array()).matrix();

      // Compute the likelihood-dependent analogue of the noise precision.
      UpdateBeta();

      // Compute the Hessian inverse, which is exactly sigma
      UpdateSigma();

      // Before progressing, check for termination based on the gradient norm
      if ( (g.array().abs()).maxCoeff() < gradient_min) {
        std::cout << "g is too small."<< std::endl;
        break;
      }

      // If all OK, compute full Newton step: H^{-1} * g
      Eigen::VectorXd delta_mu = sigma_ * g;
      int step = 1;
      
      while (step > step_min) {
        
        // Follow gradient to get new value of parameters
        Eigen::MatrixXd mu = w_ + step * delta_mu;
        t_estimate = phi_samples_ * mu;

        y = Sigmoid(t_estimate);

        // Compute outputs and error at new point
        data_error = DataError(y);
        regularizer = (alpha_.array() * (mu.array().square())).sum() / 2;
        new_total_error = data_error + regularizer;

        //auto weight_delta = (mu - w_).lpNorm<Eigen::Infinity>();
        //std::cout << "weight_delta:" << weight_delta << std::endl;

        // Test that we haven't made things worse
        if (new_total_error >= prev_error) {
          step = step / 2;
        }
        else {
          w_ = mu;
          step = 0; // end while loop
        }
      }
    } 
    return -data_error;
  }


  double DataError(Eigen::VectorXd& y) {
    
    Eigen::ArrayXXd y_n = y.array();
    Eigen::ArrayXXd t_n = training_labels_.array();

    double error = 0;
    for (int i=0; i < y.rows(); ++i) {
      if (y_n(i) == 0 || y_n(i) == 1) {
        if (t_n(i) == 1 - y_n(i))
          return std::numeric_limits<double>::infinity();
      } else 
        error += -( t_n(i)*std::log(y_n(i)) + (1-t_n(i)) * std::log(1 - y_n(i)));
    }
    return error;
  }

  Eigen::SparseVector<double> GetKernelColumn( long idx ) const {
    Eigen::SparseVector<double> out;
    out.resize(training_data_.size());
    for (long i = 0; i < training_data_.size(); i++) {
      double val = kernel_->Compute(training_data_[i], training_data_[idx]);
      if(std::abs(val) > 1e-5) {
        out.insert(i) = val;
      }
    }
    return out;
  }

  static void RemoveRow(Eigen::MatrixXd *matrix, unsigned int rowToRemove) {
    unsigned int numRows = matrix->rows()-1;
    unsigned int numCols = matrix->cols();

    if (rowToRemove < numRows) {
      matrix->block(rowToRemove,0,numRows-rowToRemove,numCols) =
        matrix->block(rowToRemove+1,0,numRows-rowToRemove,numCols).eval();
    }

    matrix->conservativeResize(numRows,numCols);
  }

  static void RemoveColumn(Eigen::MatrixXd *matrix, unsigned int colToRemove) {
    unsigned int numRows = matrix->rows();
    unsigned int numCols = matrix->cols()-1;

    if (colToRemove < numCols) {
      matrix->block(0,colToRemove,numRows,numCols-colToRemove) =
        matrix->block(0,colToRemove+1,numRows,numCols-colToRemove).eval();
    }

    matrix->conservativeResize(numRows,numCols);
  }

  static void RemoveColumn(Eigen::SparseMatrix<double> *sp, unsigned int colToRemove) {
    Eigen::SparseMatrix<double> x;
    x.conservativeResize(sp->cols(), sp->cols()-1);
    for (unsigned int i=0; i<sp->cols()-1; i++) {
      int j = 0;
      if (i < colToRemove) {
        j = i;
      } else {
        j = i + 1;
      }
      x.insert(j, i) = 1;
    }

    (*sp) = (*sp) * x;
  }


  long PickVector() {
    std::vector<std::tuple<double, int>> vector_queue;

    std::vector<int> v(training_data_.size());
    std::iota (std::begin(v), std::end(v), 0);

    // Limit how many we check
    if( v.size() > 999) {
      std::random_device rd;
      std::mt19937 g(rd());
      std::shuffle(std::begin(v), std::end(v), g);
      v.erase(v.begin()+1000, v.end());
    }

    for ( auto r : v) {
      Eigen::SparseVector<double> k_col = GetKernelColumn(r);
      double temp = k_col.dot(training_labels_);
      temp  = temp*temp / k_col.squaredNorm();
      vector_queue.push_back(std::make_tuple(temp,r));
    }
    std::sort(vector_queue.begin(), vector_queue.end(), [](std::tuple<double,int> left, std::tuple<double, int> right) {return std::get<0>(left) < std::get<0>(right);});
    auto tup = vector_queue.back();
    bool keep_searching = true;
    while( keep_searching) {
      keep_searching = false;
      tup = vector_queue.back();
      vector_queue.pop_back();
      for ( const auto x : x_m_) {
        if ( (x-training_data_[std::get<1>(tup)]).norm() < 0.001 ) {
          keep_searching = true;
        }
      }
    }

    return std::get<1>(tup);
  }

  long NextBestAlpha( const Eigen::MatrixXd& S, const Eigen::MatrixXd& Q, const std::vector<long>& active_bases,
      const bool search_all_alphas) {
  long selected_idx = -1;
  double greatest_improvement = -1;
  for (long i = 0; i < S.rows(); ++i)
  {
      double value = -1;

      // if i is currently in the active set
      if (active_bases[i] >= 0)
      {
          const long idx = active_bases[i];
          const double s = alpha_(idx,0)*S(i,0)/(alpha_(idx,0) - S(i,0));
          const double q = alpha_(idx,0)*Q(i,0)/(alpha_(idx,0) - S(i,0));
          //std::cout << "basis " << idx << " index " << i <<  " s: " << s << " q: " << q << std::endl;

          if (q*q-s > 0)
          {
              // only update an existing alpha if this is a narrow search
              if (search_all_alphas == false)
              {
                  // choosing this sample would mean doing an update of an 
                  // existing alpha value.
                  double new_alpha = s*s/(q*q-s);
                  double cur_alpha = alpha_(idx,0);
                  new_alpha = 1/new_alpha;
                  cur_alpha = 1/cur_alpha;

                  // from equation 32 in the Tipping paper 
                  value = Q(i,0)*Q(i,0)/(S(i,0) +  1/(new_alpha - cur_alpha) ) - 
                      std::log(1 + S(i,0)*(new_alpha - cur_alpha));
                  //std::cout << "Index: " << i << " Value: " << value << " new alpha: " << new_alpha << " current alpha: " << cur_alpha << " selected_idx: " << selected_idx << std::endl;
              }

          }
          // we only pick an alpha to remove if this is a wide search and it wasn't one of the recently added ones 
          else if (search_all_alphas && idx+2 < alpha_.size() )  
          {
              // choosing this sample would mean the alpha value is infinite 
              // so we would remove the selected sample from our model.

              // from equation 37 in the Tipping paper 
              value = Q(i,0)*Q(i,0)/(S(i,0) - alpha_(idx,0)) - 
                  std::log(1-S(i,0)/alpha_(idx,0));

          }
      }
      else if (false)
      {
          const double s = S(i,0);
          const double q = Q(i,0);

          if (q*q-s > 0)
          {
              // choosing this sample would mean we would add the selected 
              // sample to our model.

              // from equation 27 in the Tipping paper 
              value = (Q(i,0)*Q(i,0)-S(i,0))/S(i,0) + std::log(S(i,0)/(Q(i,0)*Q(i,0)));
          }
      }

      if (value > greatest_improvement)
      {
          greatest_improvement = value;
          selected_idx = i;
      }
  }

  // If the greatest_improvement in marginal likelihood we would get is less
  // than our epsilon then report that there isn't anything else to do.  But
  // if it is big enough then return the selected_idx.
  if (greatest_improvement > eps)
      return selected_idx;
  else {
      int value = 0;
      int return_idx = 0;
      while (value != -1) {
         std::random_device rd;
         std::mt19937 gen(rd());
         std::uniform_int_distribution<> dis(0, S.rows() -1 );
         return_idx = dis(gen);
         value = active_bases[return_idx];
      }
      return return_idx;
  }
  }

  bool Equal(const Eigen::MatrixXd& a, const Eigen::MatrixXd& b) {
    if(a.rows()!=b.rows() || a.cols() != b.cols()) {
      return false;
    }

    for(long i = 0; i< a.rows(); i++) {
      for(long j = 0; j < a.cols(); j++) {
        if(std::abs(a(i,j)-b(i,j)) > eps) {
          return false;
        }
      }
    }
    return true;
  }


  Eigen::VectorXd Sigmoid(Eigen::VectorXd& X) {
    auto Y = 1 / (1 + (-X.array()).exp());
    return Y;
  };


};



} // namespace bayesian_inference
} // namespace library
