#pragma once

#include <vector>

#include <boost/assert.hpp>

#include <Eigen/Core>

namespace library {
namespace bayesian_inference {

template <class T>
class IKernel {
 public:
  virtual double Compute(const T &sample, const T &x_m, bool debug = false) const = 0;
  virtual double GetGradient() const = 0;
};

template <class T>
class GaussianKernel : public IKernel<T> {
 public:
  GaussianKernel(double r) : radius_sq_(r*r) {}

  virtual double Compute(const T &sample, const T &x_m, bool debug = false) const {
    if(debug)
      std::cout << "Compute\n";
    BOOST_ASSERT(sample.size() == x_m.size());

    if(debug)
      std::cout << "Sq n " << (x_m-sample).squaredNorm() << std::endl;;
    double sq_n = (x_m - sample).squaredNorm();

    if(debug)
      std::cout << "exp " << std::exp(-sq_n/radius_sq_) << std::endl;
    return std::exp(-sq_n / radius_sq_);
  }

  virtual double GetGradient() const {
    return radius_sq_;
  }

 private:
  double radius_sq_;
};

template <class T>
class MaternArdKernel : public IKernel<T> {
 public:
  MaternArdKernel(int d, double sf, std::vector<double>& ells) :
    d_(d),
    sf_(sf) {
    BOOST_ASSERT(d == 1 || d == 3 || d == 5);
    // Construct the P matrix
    p_inv_ = Eigen::VectorXd(ells.size());
    for (int i = 0; i < ells.size(); ++i)
      p_inv_(i) = 1/(ells[i]*ells[i]);
    }

  virtual double Compute(const T &sample, const T &x_m, bool debug = false) const {
    if(debug)
      std::cout << "Compute\n";
    BOOST_ASSERT(sample.size() == x_m.size());
 
    double r = std::sqrt((x_m - sample).transpose() * p_inv_.asDiagonal() * (x_m - sample));
    double t = std::sqrt(d_)*r;
    double f = 1;;
    switch (d_) {
      case 1:
        f = 1;
        break;
      case 3:
        f = 1 + t;
        break;
      case 5:
        f = 1 + t + t*t/3;
        break;
    }      
    return sf_* sf_ * f * std::exp( -(std::sqrt(d_)) * r );
  }

  virtual double GetGradient() const {
    return 0.0;
  }

 private:
  int d_;
  double sf_;
  Eigen::VectorXd p_inv_;
};



} // namespace bayesian_inference
} // namespace library
