#include <iostream>
#include <iomanip>

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/filters/passthrough.h>

#include "common/timer/timer.h"
#include "classification/semantic_map_rvm.h"

namespace bi = library::bayesian_inference;
namespace tr = library::timer;


pcl::PointCloud<pcl::PointXYZL>::Ptr Kitti2Pcl(std::string& data_file) {
  std::cout << "Reading ..." << std::endl;

  pcl::PointCloud<pcl::PointXYZL>::Ptr pc(new pcl::PointCloud<pcl::PointXYZL>);
  std::ifstream data_in(data_file);

  pcl::PointXYZL point;
  while (data_in >> point.x >> point.y >> point.z >> point.label)
    pc->push_back(point);
  return pc;
}

void SaveResults(std::string& data_file, 
                 pcl::PointCloud<pcl::PointXYZL>::Ptr output_cloud, 
                 std::vector<std::vector<double>> probs) {
  std::cout << "Writing ..." << std::endl;

  std::ofstream data_out(data_file);
  if (data_out.is_open()) {
    for (size_t i = 0; i < output_cloud->points.size(); ++i) {
      data_out << float(output_cloud->points[i].x) << " "
               << float(output_cloud->points[i].y) << " "
               << float(output_cloud->points[i].z);
      for (int j = 0; j < probs.size(); ++j)  // number of class
        data_out << " " << probs[j][i];
      data_out << "\n";
    }
  }
}

void SemanticMap () {

  std::string data_path("/home/ganlu/bayesian-inference/data/data_kitti/pc_global/");
  std::string result_path("/home/ganlu/bayesian-inference/data/data_kitti/rvm_prior/");

  for (int i = 38; i < 80; ++i) {

    char buff1[100];
    char buff2[100];
    snprintf(buff1, sizeof(buff1), "%06d.txt", i);
    snprintf(buff2, sizeof(buff2), "%06d.txt", i);
    std::string input_file = data_path + std::string(buff1);
    std::string output_file = result_path + std::string(buff2);
    
    pcl::PointCloud<pcl::PointXYZL>::Ptr input_cloud = Kitti2Pcl(input_file);
    pcl::PointCloud<pcl::PointXYZL>::Ptr input_cloud_valid(new pcl::PointCloud<pcl::PointXYZL>);
    pcl::PointCloud<pcl::PointXYZL>::Ptr input_cloud_downsampled(new pcl::PointCloud<pcl::PointXYZL>);
   
    // Point Cloud Validation
    int invalid_label = 1;  // sky label 1
    for (size_t i = 0; i < input_cloud->points.size(); ++i) {
      if (input_cloud->points[i].label != invalid_label)
        input_cloud_valid->push_back(input_cloud->points[i]);
    } 
    std::cerr << "Cloud before filtering: " << input_cloud->points.size()       << std::endl;
    std::cerr << "Cloud after filtering: "  << input_cloud_valid->points.size() << std::endl;

    // Downsample points
    size_t skip = 100;
    for (size_t i = 0; i < input_cloud_valid->points.size(); ++i) {
      if (i % skip == 0)
        input_cloud_downsampled->push_back(input_cloud_valid->points[i]);
    }
    std::cerr << "Cloud before downsample: " << input_cloud_valid->points.size()       << std::endl;
    std::cerr << "Cloud after downsample: "  << input_cloud_downsampled->points.size() << std::endl;

    // Build Semantic RVM
    tr::Timer t;
    t.Start();
    bi::SemanticMapRvm<pcl::PointXYZL> semantic_map_rvm(11, input_cloud_downsampled, input_cloud_valid);
    std::cout << t.GetSeconds() << std::endl;
    
    // Get and save results
    std::vector<std::vector<double>> result = semantic_map_rvm.Predict();
    SaveResults(output_file, input_cloud_valid, result);
  }
}

int main() {

  SemanticMap();

  return 0;
}
