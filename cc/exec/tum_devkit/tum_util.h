#pragma once

#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/range.hpp>
#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>

class GTRow
{
    public:
        double const& operator[](std::size_t index) const
        {
            return m_data[index];
        }
        std::size_t size() const
        {
            return m_data.size();
        }
        void readNextRow(std::istream& str)
        {
            std::string         line = "#";
            while(line[0] == '#') {
             std::getline(str, line);
             std::cout << line << std::endl;
            }

            std::stringstream   lineStream(line);
            std::string         cell;

            m_data.clear();
            while(std::getline(lineStream, cell, ' '))
            {
                m_data.push_back(std::atof(cell.c_str()));
            }
        }
    private:
        std::vector<double>    m_data;
};


std::istream& operator>>(std::istream& str, GTRow& data)
{
  data.readNextRow(str);
  return str;
}

class TUMData {
  public:
    TUMData(std::string path, int kinect_id = 1) :
      data_dir_(path.c_str())
      {

      boost::filesystem::path depth_dir_ = data_dir_ / "depth";
      std::vector<double> depth_times;
      if(is_directory(depth_dir_)) {
        std::cout << depth_dir_ << " is a directory\n";

        for(auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(depth_dir_), {})) {
          double time = std::atof(entry.path().stem().c_str());
          depth_times.push_back(time);
          depth_files_[time] = entry;
        }
      }

      boost::filesystem::path rgb_dir_ = data_dir_ / "rgb";
      std::vector<double> rgbd_times;
      if(is_directory(rgb_dir_)) {
        std::cout << rgb_dir_ << " is a directory\n";

        for(auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(rgb_dir_), {})) {
          double time = std::atof(entry.path().stem().c_str());
          rgbd_times.push_back(time);
          rgb_files_[time] = entry;
        }
      }

      std::vector<std::tuple<double, double>> potential_matches;
      for( auto a: rgbd_times ) {
        for( auto b: depth_times ) {
          if( std::abs(a - b) < 0.017) {
            potential_matches.push_back(std::make_tuple(a,b));
          }
        }
      }
      for( auto t: potential_matches) {
        auto it_a = std::find(rgbd_times.begin(), rgbd_times.end(), std::get<0>(t));
        auto it_b = std::find(depth_times.begin(), depth_times.end(), std::get<1>(t));
        if( it_a != rgbd_times.end() && it_b != depth_times.end()) {
          matches_.push_back(t);
          rgbd_times.erase(it_a);
          depth_times.erase(it_b);
        }
      }

      std::sort(matches_.begin(), matches_.end(), [](std::tuple<double,double>a, std::tuple<double,double> b) {
          return std::get<0>(a) < std::get<0>(b);
      });



      std::ifstream gt_file((data_dir_ / "groundtruth.txt").string());
      GTRow row;
      while(gt_file >> row)
      {
        trajector_map_[row[0]] = Sophus::SE3d(Eigen::Quaterniond(row[4],row[5],row[6], row[7]),
            Eigen::Vector3d(row[1], row[2], row[3]));
        std::cout << trajector_map_[row[0]].matrix() << std::endl;
      }
      for( auto t: matches_ ) {
        std::cout << std::setprecision(16) << "RGB Time " << std::get<0>(t) << " Depth Time " << std::get<1>(t) << std::endl;
      }
      std::cout << matches_.size() << " files\n";
      std::cout << trajector_map_.size() << " poses\n";
    }

    pcl::PointCloud<pcl::PointXYZI>::Ptr
    GetPointCloudAt(const int i) {

      pcl::PointCloud<pcl::PointXYZI>::Ptr out(new pcl::PointCloud<pcl::PointXYZI>);

      auto t = matches_.at(i);

      auto rgb_f = rgb_files_.find(std::get<0>(t));
      cv::Mat grey = cv::imread(rgb_f->second.string(), cv::IMREAD_GRAYSCALE);
      if(grey.data == NULL) {
        std::cout << "Failed to read rgb " << i << std::endl;
      }
      auto depth_f = depth_files_.find(std::get<1>(t));
      cv::Mat depth = cv::imread(depth_f->second.string(), -1);
      if(depth.data == NULL) {
        std::cout << "Failed to read depth " << i << std::endl;
      }

      for(int v = 0; v < depth.rows; ++v) {
        for(int u = 0; u < depth.cols; ++u) {
          if( depth.at<uint16_t>(v,u) != 0){ 
            pcl::PointXYZI p;
            p.z = static_cast<float>(depth.at<uint16_t>(v,u)) / factor_;
            p.x = (static_cast<float>(u) -cx_)*p.z/fx_;
            p.y = (static_cast<float>(v) -cy_)*p.z/fy_;
            p.intensity = static_cast<float>(grey.at<uint8_t>(v,u));
            out->push_back(p);
          }
        }
      }
      return out;
    }

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr
    GetRGBPointCloudAt(const int i) {

      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr out(new pcl::PointCloud<pcl::PointXYZRGBA>);

      auto t = matches_.at(i);

      auto rgb_f = rgb_files_.find(std::get<0>(t));
      cv::Mat bgr = cv::imread(rgb_f->second.string(), cv::IMREAD_COLOR);
      cv::Mat bgra;
      cv::cvtColor(bgr, bgra, cv::COLOR_BGR2BGRA);
      if(bgra.data == NULL) {
        std::cout << "Failed to read rgb " << i << std::endl;
      }
      auto depth_f = depth_files_.find(std::get<1>(t));
      cv::Mat depth = cv::imread(depth_f->second.string(), -1);
      if(depth.data == NULL) {
        std::cout << "Failed to read depth " << i << std::endl;
      }


      for(int v = 0; v < depth.rows; ++v) {
        for(int u = 0; u < depth.cols; ++u) {
          if( depth.at<uint16_t>(v,u) != 0){ 
            pcl::PointXYZRGBA p;
            p.z = static_cast<float>(depth.at<uint16_t>(v,u)) / factor_;
            p.x = (static_cast<float>(u) -cx_)*p.z/fx_;
            p.y = (static_cast<float>(v) -cy_)*p.z/fy_;
            cv::Vec4b color = bgra.at<cv::Vec4d>(v,u);
            p.r = color.val[2];
            p.g = color.val[1];
            p.b = color.val[0];
            p.a = color.val[3];
            out->push_back(p);
          }
        }
      }
      return out;
    }


    Sophus::SE3d GetPoseAt(const int i) {
      auto t = matches_.at(i);
      double time = (std::get<0>(t)+std::get<1>(t))/2.0;

      auto low = trajector_map_.lower_bound(time);
      auto prev = std::prev(low);
      if((time-prev->first) < (low->first -time)) {
        std::cout << "time difference: " << prev->first -time << std::endl;
        return prev->second;
      } else {
        std::cout << "time difference: " << low->first -time << std::endl;
        return low->second;
      }
    }

    double GetTimeAt(const int i) {
      auto t = matches_.at(i);
      double time = (std::get<0>(t)+std::get<1>(t))/2.0;
      return time;
    }

    size_t Size() {
      return matches_.size();
    }

  private:
    boost::filesystem::path data_dir_;
    boost::filesystem::path accel_file_;
    boost::filesystem::path ground_truth_file_;
    std::map<double, boost::filesystem::path> depth_files_;
    std::map<double, boost::filesystem::path> rgb_files_;
    std::map<double, Sophus::SE3d> trajector_map_;
    std::vector<std::tuple<double, double>> matches_;

    float factor_ = 5000;
    float fx_ = 525;
    float fy_ = 525;
    float cx_ = 319.5;
    float cy_ = 239.5;
};
