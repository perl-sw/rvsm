#include <vector>
#include <algorithm>
#include <boost/filesystem.hpp>

// for the newly defined pointtype
#define PCL_NO_PRECOMPILE
#include <Eigen/Core>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <boost/shared_ptr.hpp>
#include <pcl/impl/point_types.hpp>

namespace pcl {
template <unsigned int NUM_CLASS>
struct PointSegmentedDistribution
{
  PCL_ADD_POINT4D;
  PCL_ADD_RGB;
  int   label;
  float label_distribution[NUM_CLASS];  // templated on any number of classes.
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW       // make sure our new allocators are aligned
} EIGEN_ALIGN16;                        // enforce SSE padding for correct memory alignment
}

namespace fs = boost::filesystem;

POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::PointSegmentedDistribution<14>,
                                (float, x, x)
                                (float, y, y)
                                (float, z, z)
                                (float, rgb, rgb)
                                (int, label, label)
                                (float[14],label_distribution, label_distribution)
)




int main() {
  std::string seq_path("/home/ganlu/Datasets/nclt/segmented_txt/");

  std::vector<fs::path> txts;
  for (auto& entry: fs::directory_iterator(seq_path))
    txts.push_back(entry.path());
  std::sort(txts.begin(), txts.end());

  for (int i = 0; i < txts.size(); ++i) {
    std::string txt_file = txts[i].string();
    std::cout << txt_file << std::endl;
    
    // write to a pcd file
    pcl::PointCloud<pcl::PointSegmentedDistribution<14>>::Ptr output_cloud(new pcl::PointCloud<pcl::PointSegmentedDistribution<14>>);
    std::ifstream infile(txt_file);
    pcl::PointSegmentedDistribution<14> point;
    float rf;
    float gf;
    float bf;
    while(infile >> point.x >> point.y >> point.z >> rf >> gf >> bf >> point.label
                 >> point.label_distribution[0]  >> point.label_distribution[1]
                 >> point.label_distribution[2]  >> point.label_distribution[3]
                 >> point.label_distribution[4]  >> point.label_distribution[5]
                 >> point.label_distribution[6]  >> point.label_distribution[7]
                 >> point.label_distribution[8]  >> point.label_distribution[9]
                 >> point.label_distribution[10] >> point.label_distribution[11]
                 >> point.label_distribution[12] >> point.label_distribution[13]) {

      uint8_t r = int(rf*255), g = int(gf*255), b = int(bf*255);    // Example: Red color
      //std::cout << (int)r << " " <<(int) g << " " <<(int) b <<std::endl;
      uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
      point.rgb = *reinterpret_cast<float*>(&rgb);
      output_cloud->push_back(point);
    } 

    std::string pcd_file = txt_file.substr(0, txt_file.size()-4) + ".pcd";
    pcl::io::savePCDFileBinary (pcd_file, *output_cloud);
    //pcl::io::savePCDFileASCII (pcd_file, *output_cloud);
  }

  return 0;
}
