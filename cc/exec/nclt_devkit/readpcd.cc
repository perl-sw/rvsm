#include <vector>
#include <algorithm>
#include <boost/filesystem.hpp>

// for the newly defined pointtype
#define PCL_NO_PRECOMPILE
#include <Eigen/Core>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <boost/shared_ptr.hpp>
#include <pcl/impl/point_types.hpp>

namespace pcl {
template <unsigned int NUM_CLASS>
struct PointSegmentedDistribution
{
  PCL_ADD_POINT4D;
  PCL_ADD_RGB;
  int   label;
  float label_distribution[NUM_CLASS];  // templated on any number of classes.
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW       // make sure our new allocators are aligned
} EIGEN_ALIGN16;                        // enforce SSE padding for correct memory alignment
}

namespace fs = boost::filesystem;

POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::PointSegmentedDistribution<14>,
                                (float, x, x)
                                (float, y, y)
                                (float, z, z)
                                (float, rgb, rgb)
                                (int, label, label)
                                (float[14],label_distribution, label_distribution)
)




int main() {
  std::string seq_path("/home/ganlu/Datasets/nclt/seg_pcds_downsampled_global (copy)/");

  std::vector<fs::path> pcds;
  for (auto& entry: fs::directory_iterator(seq_path))
    pcds.push_back(entry.path());
  std::sort(pcds.begin(), pcds.end());

  for (int i = 0; i < pcds.size(); ++i) {
    std::string pcd_file = pcds[i].string();
    std::cout << pcd_file << std::endl;
    pcl::PointCloud<pcl::PointSegmentedDistribution<14>>::Ptr input_cloud(new pcl::PointCloud<pcl::PointSegmentedDistribution<14>>);
    pcl::io::loadPCDFile<pcl::PointSegmentedDistribution<14>> (pcd_file, *input_cloud);
    std::cout << input_cloud->points.size() << std::endl;
    // write to a txt file
    std::string txt_file = pcd_file.substr(0, pcd_file.size()-4) + ".txt";
    std::ofstream data_out(txt_file);
    if (data_out.is_open()) {
      for (int j = 0; j < input_cloud->points.size(); ++j) {
        data_out << input_cloud->points[j].x << " "
                 << input_cloud->points[j].y << " "
                 << input_cloud->points[j].z << " ";
                 //<< input_cloud->points[j].rgb << " ";
        for (int  c = 0; c < 14; ++c)
          data_out << input_cloud->points[j].label_distribution[c]<<" ";
        data_out << input_cloud->points[j].label<< "\n";
      }
      data_out.close();
    }

  }

  return 0;
}
