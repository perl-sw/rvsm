#pragma once

#include <stdio.h>
#include <bitset>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

pcl::PointCloud<pcl::PointXYZI>::Ptr kitti2pcl(std::string fn) {
  std::cout << "Reading " << fn << std::endl;

  FILE* fp = std::fopen(fn.c_str(), "r");
  if (!fp) {
    std::perror("File opening failed");
  }

  std::fseek(fp, 0L, SEEK_END);
  size_t sz = std::ftell(fp);

  std::rewind(fp);

  int n_hits = sz / (sizeof(float) * 4);

  pcl::PointCloud<pcl::PointXYZI>::Ptr pc(new pcl::PointCloud<pcl::PointXYZI>);

  for (int i = 0; i < n_hits; i++) {
    pcl::PointXYZI point;

    if (fread(&point.x, sizeof(float), 1, fp) == 0) break;
    if (fread(&point.y, sizeof(float), 1, fp) == 0) break;
    if (fread(&point.z, sizeof(float), 1, fp) == 0) break;
    if (fread(&point.intensity, sizeof(float), 1, fp) == 0) break;

    pc->push_back(point);
  }

  std::fclose(fp);

  return pc;
}

template <size_t Rows, size_t Cols>
Eigen::Matrix<double, Rows, Cols> ReadCalibrationVariable(
    const std::string& prefix, std::istream& file) {
  // rewind
  file.seekg(0, std::ios::beg);
  file.clear();

  double buff[20] = {0};

  int itter = 0;

  std::string line;
  while (std::getline(file, line)) {
    if (line.empty()) continue;

    size_t found = line.find(prefix);
    if (found != std::string::npos) {
      std::cout << prefix << std::endl;
      std::cout << line << std::endl;
      std::stringstream stream(
          line.substr(found + prefix.length(), std::string::npos));
      while (!stream.eof()) {
        stream >> buff[itter++];
      }
      std::cout << std::endl;
      break;
    }
  }

  Eigen::Matrix<double, Rows, Cols> results;
  for (size_t i = 0; i < Rows; i++) {
    for (size_t j = 0; j < Cols; j++) {
      results(i, j) = buff[Cols * i + j];
    }
  }
  return results;
}

void PoseToFile(const Sophus::SE3d pose, std::ostream& file) {
  const Eigen::Matrix4d mat = pose.matrix();

  file << std::scientific << mat(0, 0) << " " << mat(0, 1) << " " << mat(0, 2)
       << " " << mat(0, 3) << " " << mat(1, 0) << " " << mat(1, 1) << " "
       << mat(1, 2) << " " << mat(1, 3) << " " << mat(2, 0) << " " << mat(2, 1)
       << " " << mat(2, 2) << " " << mat(2, 3) << std::endl;
}
