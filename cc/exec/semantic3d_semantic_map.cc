#include <iostream>
#include <iomanip>

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/filters/passthrough.h> 

#include "common/timer/timer.h"
#include "classification/semantic_map_rvm.h"


POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::PointXYZIL,
                                (float, x, x)
                                (float, y, y)
                                (float, z, z)
                                (float, intensity, intensity)
                                (uint32_t, label, label)
)

POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::PointXYZRGBLL,
                                (float, x, x)
                                (float, y, y)
                                (float, z, z)
                                (float, r, r)
                                (float, g, g)
                                (float, b, b)
                                (uint32_t, label, label)
)

POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::PointXYZIRGBL,
                                (float, x, x)
                                (float, y, y)
                                (float, z, z)
                                (float, intensity, intensity)
                                (float, r, r)
                                (float, g, g)
                                (float, b, b)
                                (uint32_t, label, label)
)

namespace bi = library::bayesian_inference;
namespace tr = library::timer;

pcl::PointCloud<pcl::PointXYZRGBLL>::Ptr Semantic3d2Pcl(std::string& pointcloud_file, std::string& label_file) {
  std::cout << "Reading ..." << std::endl;

  pcl::PointCloud<pcl::PointXYZRGBLL>::Ptr pc(new pcl::PointCloud<pcl::PointXYZRGBLL>);
  std::ifstream infile_pointcloud(pointcloud_file);
  std::ifstream infile_label(label_file);

  pcl::PointXYZRGBLL point;
  double i;
  while (infile_pointcloud >> point.x >> point.y >> point.z >> i
                           >> point.r >> point.g >> point.b && infile_label >> point.label)
  {
    pc->push_back(point);
  }
  return pc;
}

void SemanticMap ( const int sequence_idx ) {
  
  // Load raw data
  std::string label_file("/home/ganlu/labels_test_reduced_backup/test_reduced/MarketplaceFeldkirch_Station4.labels");
  std::string pointcloud_file("/home/ganlu/labels_test_reduced_backup/MarketplaceFeldkirch_Station4_rgb_intensity-reduced.txt");

  // Load raw point clouds
  pcl::PointCloud<pcl::PointXYZRGBLL>::Ptr input_cloud = Semantic3d2Pcl(pointcloud_file, label_file);
  pcl::PointCloud<pcl::PointXYZRGBLL>::Ptr input_cloud_valid(new pcl::PointCloud<pcl::PointXYZRGBLL>);
  pcl::PointCloud<pcl::PointXYZRGBLL>::Ptr input_cloud_downsampled(new pcl::PointCloud<pcl::PointXYZRGBLL>);
  pcl::io::savePCDFileASCII ("/home/ganlu/bayesian-inference/data/semantic3d/000.pcd", *input_cloud);

  // Point Cloud Validation (filter out points with label 0)
  for (size_t i = 0; i < input_cloud->points.size(); ++i) {
    if (input_cloud->points[i].label > 0)
      input_cloud_valid->push_back(input_cloud->points[i]);
  } 
  std::cerr << "Cloud before filtering: " << input_cloud->points.size()       << std::endl;
  std::cerr << "Cloud after filtering: "  << input_cloud_valid->points.size() << std::endl;

  // Downsample points
  size_t skip = 100;
  for (size_t i = 0; i < input_cloud_valid->points.size(); ++i) {
    if (i % skip == 0)
      input_cloud_downsampled->push_back(input_cloud_valid->points[i]);
  }
  std::cerr << "Cloud before downsample: " << input_cloud_valid->points.size()       << std::endl;
  std::cerr << "Cloud after downsample: "  << input_cloud_downsampled->points.size() << std::endl;

  // Build Semantic RVM
  tr::Timer t;
  t.Start();
  bi::SemanticMapRvm<pcl::PointXYZRGBLL> semantic_map_rvm(8, input_cloud_downsampled, input_cloud_downsampled);
  pcl::io::savePCDFileASCII ("/home/ganlu/bayesian-inference/data/semantic3d/input_cloud.pcd", *input_cloud_downsampled);
  std::cout << t.GetSeconds() << std::endl;
  
  pcl::PointCloud<pcl::PointXYZRGBLL>::Ptr output_cloud = semantic_map_rvm.Predict();
  pcl::io::savePCDFileASCII ("/home/ganlu/bayesian-inference/data/semantic3d/15_000000_pred.pcd", *output_cloud);

}


int main() {

  SemanticMap(9);

  return 0;
}
