#include <iostream>
#include <fstream>
#include <random>
#include <chrono>
#include <cmath>

#include <Eigen/Core>
#include <Eigen/Dense>

#include "common/timer/timer.h"

#include "rvm/rvm.h"
#include "rvm/kernel.h"

#include "rvm/rvm_classification.h"

namespace bi = library::bayesian_inference;
namespace tr = library::timer;


  Eigen::VectorXd Sigmoid(Eigen::VectorXd& X) {
    auto Y = 1 / (1 + (-X.array()).exp());
    return Y;
  };

int main(int argc, char **argv) {
  printf("RVM Test\n");
  printf("./bin [training samples] [test samples] [dimension] [iterations]\n");

  int n_train_samples = 900;
  int n_test_samples = 900;
  int dim = 2;
  int iterations = 100;

  if (argc > 1) {
    n_train_samples = atoi(argv[1]);
  }

  if (argc > 2) {
    n_test_samples = atoi(argv[2]);
  }

  if (argc > 3) {
    dim = atoi(argv[3]);
  }

  if (argc > 4) {
    iterations = atoi(argv[4]);
  }



  tr::Timer t;

  printf("%d training samples, %d test samples, dimensionality %d, %d iterations\n",
      n_train_samples, n_test_samples, dim, iterations);

  std::vector<Eigen::VectorXd> train_data;
  std::vector<int> train_labels;

  std::vector<Eigen::VectorXd> test_data;
  std::vector<int> test_labels;

  /*std::ifstream infile("/home/ganlu/bayesian-inference/cc/targets.csv");
  std::string line;
  while (std::getline(infile, line)) {
    std::istringstream iss(line);
    int t;
    if (!(iss >> t)) break; // error
    train_labels.push_back(t);
  }*/

  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine rand_engine(seed);

  // Generate samples
  std::normal_distribution<double> n_distribution(0, 1.0);
  std::uniform_real_distribution<double> u_distribution(0, 1.0);

  // Gives a nice square grid of decent size
  double sqrtN = std::sqrt(n_train_samples);
  int n = 0;
  Eigen::MatrixXd X(n_train_samples, dim);
  for (int i = 0; i < sqrtN; ++i) {
    for (int j = 0; j < sqrtN; ++j) {
      X(n, 0) = double(i) / sqrtN;
      X(n, 1) = double(j) / sqrtN;
      n++;
    }
  }
  //std::cout << X << std::endl;

  // Heuristically adjust basis width to account for distance scaling with dimension.
  double basis_width = 0.05;
  basis_width = std::pow(basis_width, 1.0 / dim);

  // Compute squared exponential basis (design) matrix
  Eigen::MatrixXd Dist = (X.array().pow(2).matrix()).rowwise().sum() * Eigen::Matrix<double, 1, 900>::Ones() + 
    Eigen::Matrix<double, 900, 1>::Ones() * ((X.array().pow(2).matrix()).rowwise().sum()).transpose() - 2*X*X.transpose();

  Eigen::MatrixXd Basis = (-Dist.array() / std::pow(basis_width, 2)).exp();
  //std::cout << Basis << std::endl;


  double p_sparse = 0.9;
  Eigen::Matrix<double, 900, 1> w;
  for (int i = 0 ; i < n_train_samples; ++i) {
    if (u_distribution(rand_engine) < p_sparse)
      w(i, 0) = 0;
    else
      w(i, 0) = n_distribution(rand_engine) * 100.0 / 90;
  }
  //std::cout << w << std::endl;

  Eigen::VectorXd z = Basis * w;

  Eigen::VectorXd sigmoid = Sigmoid(z);
  Eigen::Matrix<int, 900, 1> Targets;
  for (int i = 0; i < n_train_samples; ++i) {
    if (u_distribution(rand_engine) < sigmoid(i) )
      Targets(i, 0) = 1;
    else
      Targets(i, 0) = 0;
  }

  //std::cout << Targets << std::endl;



  for (int i=0; i<n_train_samples; i++) {
    Eigen::VectorXd sample(dim);

    for (int j=0; j<dim; j++) {
      sample(j) = X(i, j);
    }
    train_data.push_back(sample);

    //double x = sample(0);
    //double y = sample(1);
    //double val = 0.2 * std::sin(2*M_PI*x);

    //test_labels.push_back(y > val ? 1 : 0);
    train_labels.push_back(Targets(i, 0));
  }

  test_data = train_data;
  test_labels = train_labels;

  // Generate model
  bi::GaussianKernel<Eigen::VectorXd> kernel(0.2236);
  /*bi::RvmClassifier<Eigen::VectorXd> model(train_data, train_labels, &kernel);

  t.Start();
  model.BatchSolve(iterations);
  printf("Took %5.3f sec to train RVM\n", t.GetSeconds());

  t.Start();
  std::vector<double> pred_labels = model.PredictLabels(test_data);
  printf("Took %5.3f ms to predict %d labels\n", t.GetMs(), n_test_samples);*/

  // Save to csv files
  std::ofstream data_file("data.csv");
  for (const auto &test_sample : test_data) {
    data_file << test_sample(0) << "," << test_sample(1) << std::endl;
  }
  data_file.close();

  /*std::ofstream label_file("labels.csv");
  for (int i=0; i<n_test_samples; i++) {
    label_file << test_labels[i] << "," << pred_labels[i] << std::endl;
  }
  label_file.close();

  std::ofstream xm_file("xm.csv");
  std::vector<Eigen::VectorXd> x_ms = model.GetRelevanceVectors();
  for (const auto &x_m : x_ms) {
    xm_file << x_m(0) << "," << x_m(1) << std::endl;
  }
  xm_file.close();*/


  // Generate sequential model
  bi::RvmClassification<Eigen::VectorXd> model2(train_data, train_labels, &kernel);

  t.Start();
  model2.Solve(iterations);
  printf("Took %5.3f sec to sequential train RVM\n", t.GetSeconds());

  t.Start();
  std::vector<double> pred_labels2 = model2.PredictLabels(test_data);
  printf("Took %5.3f ms to predict %d labels\n", t.GetMs(), n_test_samples);
  
  std::ofstream label_file2("labels2.csv");
  int acc = 0;
  for (int i=0; i<n_test_samples; i++) {
    label_file2 << test_labels[i] << "," << pred_labels2[i] << std::endl;
    if (pred_labels2[i] > 0.5 && test_labels[i] == 1)
      acc++;
    else if (pred_labels2[i] < 0.5 && test_labels[i] == 0)
      acc++;
  }
  std::cout << acc << std::endl;
  label_file2.close();

  std::ofstream xm_file2("xm2.csv");
  std::vector<Eigen::VectorXd> x_ms2 = model2.GetRelevanceVectors();
  for (const auto &x_m : x_ms2) {
    xm_file2 << x_m(0) << "," << x_m(1) << std::endl;
  }
  xm_file2.close();

   
  std::shared_ptr<bi::RvmClassification<Eigen::VectorXd>::RvmClassificationDecisionFunction> rvm = model2.GetDecisionFunction();
  t.Start();
  std::vector<double> pred_labels22 = rvm->PredictLabels(test_data);
  printf("Took %5.3f ms to predict %d labels\n", t.GetMs(), n_test_samples);

  acc = 0;
  for (int i=0; i<n_test_samples; i++) {
    label_file2 << test_labels[i] << "," << pred_labels2[i] << std::endl;
    if (pred_labels2[i] > 0.5 && test_labels[i] == 1)
      acc++;
    else if (pred_labels2[i] < 0.5 && test_labels[i] == 0)
      acc++;
  }
  std::cout << acc << std::endl;
  label_file2.close();
 
  return 0;
}
