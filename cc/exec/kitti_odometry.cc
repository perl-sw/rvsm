#include <iostream>
#include <memory>
#include <random>

#include <pcl/point_cloud.h>
#include <pcl/common/transforms.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/registration/ndt.h>
#include <sophus/se3.hpp>
#include <tbb/tbb.h>

#include "common/timer/timer.h"
#include "registration/gicp.h"
#include "registration/gicp_rvm.h"
#include "exec/kitti_devkit/kitti_util.h"

namespace bi = library::bayesian_inference;
namespace tr = library::timer;

void ComputeOdometry ( const int sequence_idx ) {
  float millisseconds = 0;

  int sizes[11] = {4540, 1100, 4660, 800,  270, 2760,
                   1100, 1100, 4070, 1590, 1200};

  std::ostringstream oss;
  oss << std::setw(2) << std::setfill('0') << sequence_idx;

  std::string sequence_path =
      "/home/sparki/data/kitti/dataset/sequences/" + oss.str() + "/velodyne/";
  std::ofstream gicp_results_file("/home/sparki/data/kitti/dataset/results_gicp/INIT" + oss.str() + ".txt");
  std::ofstream gicp_time_file("/home/sparki/data/kitti/dataset/results_gicp/TIME" + oss.str() + ".txt");
  std::ofstream rvm_results_file("/home/sparki/data/kitti/dataset/results_rvm/INIT" + oss.str() + ".txt");
  std::ofstream rvm_time_file("/home/sparki/data/kitti/dataset/results_rvm/TIME" + oss.str() + ".txt");
  std::ofstream pcl_results_file("/home/sparki/data/kitti/dataset/results_ndt/INIT" + oss.str() + ".txt");
  std::ofstream pcl_time_file("/home/sparki/data/kitti/dataset/results_ndt/TIME" + oss.str() + ".txt");
  std::ifstream calib_file(
      "/home/sparki/data/kitti/dataset/sequences/00/calib.txt");
  Eigen::Matrix4d calib_mat = Eigen::Matrix4d::Identity();
  calib_mat.block<3, 4>(0, 0) =
      ReadCalibrationVariable<3, 4>("Tr:", calib_file);
  std::cout << calib_mat << std::endl;
  Sophus::SE3d Lidar_to_camera = Sophus::SE3d::fitToSE3(calib_mat);
  calib_file.close();


  bi::GicpRvm::PointCloudPtr target_cloud;
  bi::GicpRvm::KdTreePtr target_kdtree;
  bi::GicpRvm::MatricesVectorPtr target_covs;
  bi::GicpRvm::RvmDecisionFunctionPtr target_rvm;


  Sophus::SE3d init_gicp;
  Sophus::SE3d toInitial_gicp;
  Sophus::SE3d init_rvm;
  Sophus::SE3d toInitial_rvm;
  Sophus::SE3d init_pcl;
  Sophus::SE3d toInitial_pcl;
  PoseToFile(toInitial_gicp, gicp_results_file);
  PoseToFile(toInitial_rvm, rvm_results_file);
  PoseToFile(toInitial_pcl, pcl_results_file);
  for (int i = 0; i < sizes[sequence_idx]; i += 1) {
    char buff1[100];
    char buff2[100];
    snprintf(buff1, sizeof(buff1), "%06d.bin", i);
    snprintf(buff2, sizeof(buff2), "%06d.bin", i + 1);
    std::string file1 = sequence_path + std::string(buff1);
    std::string file2 = sequence_path + std::string(buff2);

    if( i == 0) {
      target_cloud = kitti2pcl(file1);
      pcl::transformPointCloud(*target_cloud, *target_cloud, Lidar_to_camera.matrix());
    }
    std::cout << "Target size: " << target_cloud->size() << std::endl;


    pcl::PointCloud<pcl::PointXYZI>::Ptr source_cloud = kitti2pcl(file2);
    pcl::transformPointCloud(*source_cloud, *source_cloud, Lidar_to_camera.matrix());
    std::cout << "Source size: " << source_cloud->size() << std::endl;

    pcl::PointCloud<pcl::PointXYZI>::Ptr final_cloud(new pcl::PointCloud<pcl::PointXYZI>);

    tr::Timer t;

    //pcl::GeneralizedIterativeClosestPoint<pcl::PointXYZI, pcl::PointXYZI> pcl_ndt;
    pcl::NormalDistributionsTransform<pcl::PointXYZI, pcl::PointXYZI> pcl_ndt;
    t.Start();
    pcl_ndt.setTransformationEpsilon(1e-4);
    //pcl_ndt.setStepSize(0.1);
    //pcl_ndt.setResolution(2.0);
    pcl_ndt.setMaximumIterations(50);
    pcl_ndt.setInputSource( source_cloud );
    pcl_ndt.setInputTarget( target_cloud );

    pcl_ndt.align(*final_cloud, (init_pcl.matrix().cast<float>()));
    pcl_time_file << t.GetSeconds() << std::endl;

    toInitial_pcl = toInitial_pcl* Sophus::SE3d::fitToSE3(pcl_ndt.getFinalTransformation().cast<double>());
    PoseToFile(toInitial_pcl, pcl_results_file);

    bi::GICP<pcl::PointXYZI> gicp;
    t.Start();
    gicp.setSourceCloud( source_cloud );
    gicp.setTargetCloud( target_cloud );

    gicp.align(final_cloud, init_gicp);
    gicp_time_file << t.GetSeconds() << std::endl;

    toInitial_gicp = toInitial_gicp * gicp.getFinalTransformation();
    PoseToFile(toInitial_gicp, gicp_results_file);

    bi::GicpRvm gicp_rvm;
    t.Start();
    gicp_rvm.setSourceCloud( source_cloud );
    if( i == 0 ) {
      gicp_rvm.setTargetCloud( target_cloud );
    } else {
      gicp_rvm.setTargetCloud( target_cloud, target_kdtree, target_covs, target_rvm);
    }

    gicp_rvm.align(final_cloud, init_rvm);
    rvm_time_file << t.GetSeconds() << std::endl;

    toInitial_rvm = toInitial_rvm * gicp_rvm.getFinalTransformation();
    PoseToFile(toInitial_rvm, rvm_results_file);

    gicp_rvm.getSourceCloud(target_cloud, target_kdtree, target_covs, target_rvm);

    //target_cloud = source_cloud;

  }
}

int main() {

  //tbb::task_scheduler_init init(6);
  //tbb::parallel_for<int>( 0, 11, 1, ComputeOdometry );
  ComputeOdometry(9);

  return 0;
}
