#include <iostream>
#include <fstream>
#include <random>
#include <chrono>
#include <cmath>

#include <Eigen/Core>
#include <pcl/point_cloud.h>

#include "common/timer/timer.h"

#include "registration/gicp.h"
#include "registration/gicp_rvm.h"

namespace bi = library::bayesian_inference;
namespace tr = library::timer;

int main(int argc, char **argv) {
  printf("RVM Test\n");
  printf("./bin [training samples] [test samples] [dimension] [iterations]\n");

  int n_train_samples = 250;
  int n_test_samples = 1000;
  int dim = 2;
  int iterations = 100;

  if (argc > 1) {
    n_train_samples = atoi(argv[1]);
  }

  if (argc > 2) {
    n_test_samples = atoi(argv[2]);
  }

  tr::Timer t;

  printf("%d training samples, %d test samples\n",
      n_train_samples, n_test_samples);

  pcl::PointCloud<pcl::PointXYZI>::Ptr target_cloud(new pcl::PointCloud<pcl::PointXYZI>);
  pcl::PointCloud<pcl::PointXYZI>::Ptr source_cloud(new pcl::PointCloud<pcl::PointXYZI>);
  pcl::PointCloud<pcl::PointXYZI>::Ptr final_cloud(new pcl::PointCloud<pcl::PointXYZI>);


  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine rand_engine(seed);

  // Generate samples
  //std::normal_distribution<double> distribution(0, 0.3);
  std::uniform_real_distribution<> distribution(-0.75, 0.75);

  for (int i=0; i<n_train_samples; i++) {
    pcl::PointXYZI p;

    p.x = distribution(rand_engine);
    p.y = distribution(rand_engine);
    p.z = distribution(rand_engine);
    p.intensity = distribution(rand_engine);

    target_cloud->push_back(p);
  }

  for (int i=0; i<n_test_samples; i++) {
    pcl::PointXYZI p;

    p.x = distribution(rand_engine);
    p.y = distribution(rand_engine);
    p.z = distribution(rand_engine);
    p.intensity = distribution(rand_engine);

    source_cloud->push_back(p);
  }

  bi::GICP<pcl::PointXYZI> gicp;
  t.Start();
  gicp.setSourceCloud( source_cloud );
  gicp.setTargetCloud( target_cloud );

  gicp.align(final_cloud);
  printf("Took %5.3f sec to align\n", t.GetSeconds());

  bi::GicpRvm gicp_rvm;
  t.Start();
  gicp_rvm.setSourceCloud( source_cloud );
  gicp_rvm.setTargetCloud( target_cloud );

  gicp_rvm.align(final_cloud);
  printf("Took %5.3f sec to align\n", t.GetSeconds());

  return 0;
}
