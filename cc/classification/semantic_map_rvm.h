#pragma once

#include <Eigen/Dense>
#include <pcl/point_cloud.h>

// for the newly defined pointtype
#define PCL_NO_PRECOMPILE
#include <Eigen/Core>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <boost/shared_ptr.hpp>
#include <pcl/impl/point_types.hpp>

#include "rvm/rvm_classification.h"
#include "common/timer/timer.h"
#include "classification/semantic_map_rvm.h"

namespace pcl {
struct PointXYZIL
{
  PCL_ADD_POINT4D;
  float intensity;
  uint32_t   label;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
} EIGEN_ALIGN16;                    // enforce SSE padding for correct memory alignment}

struct PointXYZRGBLL
{
  PCL_ADD_POINT4D;
  float r;
  float g;
  float b;
  uint32_t   label;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
} EIGEN_ALIGN16;

struct PointXYZIRGBL
{
  PCL_ADD_POINT4D;
  float intensity;
  float r;
  float g;
  float b;
  uint32_t   label;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
} EIGEN_ALIGN16;
}

namespace library {
  namespace bayesian_inference {
    template <typename PointT>
    class SemanticMapRvm
    {
      public:
        typedef pcl::PointCloud<PointT> PointCloud;
        typedef typename PointCloud::Ptr PointCloudPtr;

        typedef RvmClassification<Eigen::VectorXd>::RvmClassificationDecisionFunction RvmDecisionFunction;
        typedef std::shared_ptr< RvmDecisionFunction > RvmDecisionFunctionPtr;

        SemanticMapRvm(uint32_t n, PointCloudPtr input_cloud, PointCloudPtr output_cloud) :
        class_number_(n),
        input_cloud_(input_cloud),
        output_cloud_(output_cloud)
        {
          MultiClassification();
        }

        std::vector<std::vector<double>> Predict() {
          // Prepare test data
          std::vector<Eigen::VectorXd> test_data;
          for (const auto& p: *output_cloud_) {
            Eigen::VectorXf point(3);
            point << p.x, p.y, p.z;
            test_data.push_back(point.cast<double>());
          }
          std::cout << "test data assembled" << std::endl;

          // Get predicted results for binary class
          std::vector<std::vector<double>> predict_probs;
          for (uint32_t i = 0; i < binary_rvms_.size(); ++i) {
            std::vector<double> probs;
            for (int j = 0; j < test_data.size(); ++j) {
              double prob = binary_rvms_[i]->PredictLabel(test_data[j]);
              probs.push_back(prob);
            }
            predict_probs.push_back(probs);
          }
          std::cout << "get predicted results for binary classes" << std::endl;

          // Get predict label
          SoftLabels(predict_probs);
          for (int i = 0; i < test_data.size(); ++i) {
            output_cloud_->points[i].label = HardLabel(predict_probs, i);
          }
          std::cout << "get predict labels" << std::endl;
          return predict_probs;
        }

      private:

        inline void
        BinaryClassification(uint32_t positive_label) {
          std::vector<Eigen::VectorXd> train_data;
          std::vector<int> train_labels;
          for (const auto& p: *input_cloud_) {
            Eigen::VectorXd point(3);
            point << p.x, p.y, p.z;
            train_data.push_back(point.cast<double>());
            if (p.label == positive_label)
              train_labels.push_back(1);
            else
              train_labels.push_back(0);
          }
          std::cout << "Training data assembled\n";
          
          //GaussianKernel<Eigen::VectorXd> *kernel = new GaussianKernel<Eigen::VectorXd>(0.3684);
          std::vector<double> ells = {1.8096, 1.6246, 3.9090};
          MaternArdKernel<Eigen::VectorXd> *kernel = new MaternArdKernel<Eigen::VectorXd>(5, 4.8987, ells);
          std::cout << "Kernel \n";
          RvmClassification<Eigen::VectorXd> model(train_data, train_labels, kernel);
          std::cout << "Solving\n";
          model.Solve(1000);
          binary_rvms_.push_back(model.GetDecisionFunction());
          return;
        }

        void MultiClassification() {
          for (uint32_t i = 0; i < class_number_; ++i) {
            BinaryClassification(i);
          }
          std::cout << "binary_rvms_: " << binary_rvms_.size() << std::endl;
        }

        void SoftLabels(std::vector<std::vector<double>> &probs) {
          // Soft labels
          for (int i = 0; i < probs[0].size(); ++i) {  // iterate each point
            double sum = 0;
            for (int j = 0; j < probs.size(); ++j) { // iterate each class
              probs[j][i] = std::exp(probs[j][i]);
              sum += probs[j][i];
            }
            for (int j = 0; j < probs.size(); ++j) 
              probs[j][i] = probs[j][i] / sum;
          }
          return;
        }

        uint32_t HardLabel(std::vector<std::vector<double>> &probs, int count) {
          uint32_t label = 0;
          double max = -std::numeric_limits<double>::infinity();
          for (uint32_t i = 0; i < binary_rvms_.size(); ++i) {
            if (probs[i][count] >= max) {
              max = probs[i][count];
              label = i;
            }
          }
          return label;
        }

      protected:
        uint32_t class_number_;
        PointCloudPtr input_cloud_;
        PointCloudPtr output_cloud_;
        std::vector<RvmDecisionFunctionPtr> binary_rvms_;
    
    };
  
  } // namespace bayesian_inference
} // namespace library
