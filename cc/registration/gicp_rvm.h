#pragma once

#include <Eigen/Geometry>
#include <sophus/se3.hpp>
#include <sophus/types.hpp>
#include <sophus/common.hpp>
#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree_flann.h>

#include "rvm/rvm.h"


namespace library {
  namespace bayesian_inference {
    class GicpRvm
    {
        public:
        typedef pcl::PointCloud<pcl::PointXYZI> PointCloud;
        typedef typename PointCloud::Ptr PointCloudPtr;

        typedef std::vector< Eigen::Matrix3d, Eigen::aligned_allocator<Eigen::Matrix3d> > MatricesVector;
        typedef std::vector< Eigen::Matrix<double,6,6>,
                             Eigen::aligned_allocator<Eigen::Matrix<double,6,6> > >
                                 CovarianceVector;

        typedef std::shared_ptr< MatricesVector > MatricesVectorPtr;
        typedef std::shared_ptr< const MatricesVector > MatricesVectorConstPtr;

        typedef pcl::KdTreeFLANN<pcl::PointXYZI> KdTree;
        typedef typename KdTree::Ptr KdTreePtr;

        typedef RvmRegression<Eigen::Vector3d>::RvmRegressionDecisionFunction RvmDecisionFunction;
        typedef std::shared_ptr< RvmDecisionFunction > RvmDecisionFunctionPtr;

        typedef Eigen::Matrix<double, 6, 1> Vector6d;

        std::vector< Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > convergeTransforms;
        std::vector< double > convergeTime;

        GicpRvm(int k = 20, double epsilon = 1e-3) :
        kCorrespondences_(k),
        epsilon_(epsilon)
        {
            Eigen::Matrix4d mat = Eigen::Matrix4d::Identity();
            baseTransformation_ = Sophus::SE3d(mat);
        };

        inline void
        setSourceCloud( const PointCloudPtr &cloud ) {
            std::cout << "Setting Source\n";
            sourceCloud_ = cloud;
            sourceKdTree_ = KdTreePtr(new KdTree());
            sourceKdTree_->setInputCloud(sourceCloud_);
            sourceCovariances_ = MatricesVectorPtr(new MatricesVector());
            computeCovariances(sourceCloud_, sourceKdTree_, sourceCovariances_);

            // RVM
            std::vector<Eigen::Vector3d> train_data;
            std::vector<double> train_labels;
            for(const auto& p : *sourceCloud_) {
              train_data.push_back(p.getVector3fMap().cast<double>());
              train_labels.push_back(static_cast<double>(p.intensity));
            }
            std::cout << "Training data assembled\n";
            GaussianKernel<Eigen::Vector3d> *kernel = new GaussianKernel<Eigen::Vector3d>(2.5);
            RvmRegression<Eigen::Vector3d> model(train_data, train_labels, kernel);
            std::cout << "Solving\n";
            model.SequentialSolve(99);
            source_rvm_ = model.GetDecisionFunction();
        };

        inline void
        setSourceCloud( const PointCloudPtr &cloud, const KdTreePtr &kdtree,
            const MatricesVectorPtr &matrix_vec, const RvmDecisionFunctionPtr &rvm) {
          sourceCloud_ = cloud;
          sourceKdTree_ = kdtree;
          sourceCovariances_ = matrix_vec;
          source_rvm_ = rvm;
        };

        inline void
        getSourceCloud( PointCloudPtr &cloud, KdTreePtr &kdtree,
            MatricesVectorPtr &matrix_vec, RvmDecisionFunctionPtr &rvm) {
          cloud = sourceCloud_;
          kdtree = sourceKdTree_;
          matrix_vec = sourceCovariances_;
          rvm = source_rvm_;
        };


        inline void
        setTargetCloud ( const PointCloudPtr &cloud ) {
            std::cout << "Setting Target\n";
            targetCloud_ = cloud;
            targetKdTree_ = KdTreePtr(new KdTree());
            targetKdTree_->setInputCloud(targetCloud_);
            targetCovariances_ = MatricesVectorPtr(new MatricesVector());
            computeCovariances(targetCloud_, targetKdTree_, targetCovariances_);

            // RVM
            std::vector<Eigen::Vector3d> train_data;
            std::vector<double> train_labels;
            for(const auto& p : *sourceCloud_) {
              train_data.push_back(p.getVector3fMap().cast<double>());
              train_labels.push_back(static_cast<double>(p.intensity));
            }
            GaussianKernel<Eigen::Vector3d> *kernel = new GaussianKernel<Eigen::Vector3d>(2.5);
            RvmRegression<Eigen::Vector3d> model(train_data, train_labels, kernel);
            std::cout << "Solving\n";
            model.SequentialSolve(99);
            target_rvm_ = model.GetDecisionFunction();
        };

        inline void
        setTargetCloud( const PointCloudPtr &cloud, const KdTreePtr &kdtree,
            const MatricesVectorPtr &matrix_vec, const RvmDecisionFunctionPtr &rvm) {
          targetCloud_ = cloud;
          targetKdTree_ = kdtree;
          targetCovariances_ = matrix_vec;
          target_rvm_ = rvm;
        };

        inline void
        getTargetCloud( PointCloudPtr &cloud, KdTreePtr &kdtree,
            MatricesVectorPtr &matrix_vec, RvmDecisionFunctionPtr &rvm) {
          cloud = targetCloud_;
          kdtree = targetKdTree_;
          matrix_vec = targetCovariances_;
          rvm = target_rvm_;
        };


        void
        align(PointCloudPtr finalCloud);

        void
        align(PointCloudPtr finalCloud, const Sophus::SE3d &initTransform);

        Sophus::SE3d
        getFinalTransformation() {
            Sophus::SE3d temp = finalTransformation_;
            return temp;
        };

        int
        getOuterIter(){
            return outer_iter;
        }

        protected:

        int kCorrespondences_;
        double epsilon_;
        double translationEpsilon_;
        double rotationEpsilon_;
        int maxInnerIterations_;

        int outer_iter;

        Sophus::SE3d baseTransformation_;
        Sophus::SE3d finalTransformation_;

        PointCloudPtr sourceCloud_;
        KdTreePtr sourceKdTree_;
        MatricesVectorPtr sourceCovariances_;
        RvmDecisionFunctionPtr source_rvm_;

        PointCloudPtr targetCloud_;
        KdTreePtr targetKdTree_;
        MatricesVectorPtr  targetCovariances_;
        RvmDecisionFunctionPtr target_rvm_;

        void computeCovariances(const PointCloudPtr cloudptr, KdTreePtr treeptr, MatricesVectorPtr matvec);

        class GicpRvmCostFunction: public ceres::FirstOrderFunction {
         public:
          GicpRvmCostFunction(const PointCloudPtr source_cloud,
                              const PointCloudPtr target_cloud,
                              const MatricesVectorPtr source_covs,
                              const MatricesVectorPtr target_covs,
                              const RvmDecisionFunctionPtr source_rvm,
                              const RvmDecisionFunctionPtr target_rvm,
                              //double alpha = 8.0,
                              //double lambda = 1e-2) :
                              double alpha = 10.5,
                              double lambda = 5e-5) :
            source_cloud_(source_cloud),
            target_cloud_(target_cloud),
            source_covs_(source_covs),
            target_covs_(target_covs),
            source_rvm_(source_rvm),
            target_rvm_(target_rvm),
            alpha_(alpha),
            lambda_(lambda) {}

          virtual bool Evaluate(const double* parameters, double* cost, double* gradient) const;

          virtual int NumParameters() const { return 7; }

         private:
          PointCloudPtr source_cloud_;
          MatricesVectorPtr source_covs_;
          RvmDecisionFunctionPtr source_rvm_;

          PointCloudPtr target_cloud_;
          MatricesVectorPtr  target_covs_;
          RvmDecisionFunctionPtr target_rvm_;

          double alpha_;
          double lambda_;

          Eigen::Quaterniond dRtodq(const Eigen::Matrix3d& dR,
                                    const Eigen::Quaterniond& q,
                                    const Eigen::Matrix3d& R)  const;
        };
    };
 } // namespace labrary
} // namespace bayesian_inference
