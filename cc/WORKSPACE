new_http_archive(
    name = "boost",
    build_file = "tools/boost.BUILD",
    type = "tar.bz2",
    url = "http://robots.engin.umich.edu/~sparki/boost_1_68_0.tar.bz2",
)

# External dependency: Eigen; has no Bazel build.
new_http_archive(
    name = "com_github_eigen_eigen",
    build_file = "tools/eigen.BUILD",
    sha256 = "dd254beb0bafc695d0f62ae1a222ff85b52dbaa3a16f76e781dce22d0d20a4a6",
    strip_prefix = "eigen-eigen-5a0156e40feb",
    url = "http://bitbucket.org/eigen/eigen/get/3.3.4.tar.bz2",
)

# Google benchmark.
http_archive(
    name = "com_github_google_benchmark",
    url = "https://github.com/google/benchmark/archive/16703ff83c1ae6d53e5155df3bb3ab0bc96083be.zip",
    strip_prefix = "benchmark-16703ff83c1ae6d53e5155df3bb3ab0bc96083be",
    sha256 = "59f918c8ccd4d74b6ac43484467b500f1d64b40cc1010daa055375b322a43ba3",
)

# External dependency: Google Flags; has Bazel build already.
http_archive(
    name = "com_github_gflags_gflags",
    sha256 = "6e16c8bc91b1310a44f3965e616383dbda48f83e8c1eaa2370a215057b00cabe",
    strip_prefix = "gflags-77592648e3f3be87d6c7123eb81cbad75f9aef5a",
    url = "https://github.com/gflags/gflags/archive/77592648e3f3be87d6c7123eb81cbad75f9aef5a.tar.gz",
)

# External dependency: Google Log; has Bazel build already.
http_archive(
    name = "com_github_google_glog",
    sha256 = "7083af285bed3995b5dc2c982f7de39bced9f0e6fd78d631f3285490922a0c3d",
    strip_prefix = "glog-3106945d8d3322e5cbd5658d482c9ffed2d892c0",
    url = "https://github.com/drigz/glog/archive/3106945d8d3322e5cbd5658d482c9ffed2d892c0.tar.gz",
)

http_archive(
    name = "com_google_ceres_solver",
    sha256 = "1296330fcf1e09e6c2f926301916f64d4a4c5c0ff12d460a9bc5d4c48411518f",
    strip_prefix = "ceres-solver-1.14.0",
    url = "https://github.com/ceres-solver/ceres-solver/archive/1.14.0.tar.gz",
)

new_http_archive(
    name = "flann",
    build_file = "tools/flann.BUILD",
    sha256 = "b23b5f4e71139faa3bcb39e6bbcc76967fbaf308c4ee9d4f5bfbeceaa76cc5d3",
    strip_prefix = "flann-1.9.1",
    url = "https://github.com/mariusmuja/flann/archive/1.9.1.tar.gz",
)

new_git_repository(
    name = "sophus",
    build_file = "tools/sophus.BUILD",
    commit = "13fb3288311485dc94e3226b69c9b59cd06ff94e",
    remote = "https://github.com/strasdat/Sophus.git",
)

new_http_archive(
    name = "pcl",
    build_file = "tools/pcl.BUILD",
    sha256 = "5a102a2fbe2ba77c775bf92c4a5d2e3d8170be53a68c3a76cfc72434ff7b9783",
    strip_prefix = "pcl-pcl-1.8.1",
    url = "https://github.com/PointCloudLibrary/pcl/archive/pcl-1.8.1.tar.gz",
)

new_http_archive(
    name = "com_github_01org_tbb",
    build_file = "tools/tbb.BUILD",
    sha256 = "d420bb5a780585fdab0988f25172650b5ebf6b35503318b7e608e99c0bbaed99",
    url = "https://github.com/01org/tbb/releases/download/2018_U5/tbb2018_20180618oss_lin.tgz",
    strip_prefix = "tbb2018_20180618oss",
)

new_http_archive(
    name = "opencv",
    url = "https://github.com/opencv/opencv/archive/3.1.0.tar.gz",
    strip_prefix = "opencv-3.1.0",
    build_file = "tools/opencv.BUILD",
)


