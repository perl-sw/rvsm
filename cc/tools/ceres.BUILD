cc_library(
    name = "ceres",
    srcs = glob(
        [
            "internal/**/*.h",
            "internal/**/*.cc",
        ],
        exclude = [
            "internal/ceres/gtest/**",
            "internal/ceres/test_util.*",
            "internal/ceres/*_test_utils.*",
            "**/*_test.cc",
            "**/*_gtest_all.cc",
            "internal/ceres/gmock_main.cc",
            "internal/ceres/miniglog/**",
        ],
    ),
    hdrs = glob(["include/ceres/**"]),
    copts = [
        "-O2",
        "-Wno-sign-compare",
        "-Wno-maybe-uninitialized",
        "-Iexternal/ceres/internal",

        # These version strings are used solely for debugging output.
        # Select a value which would likely cause compilation errors
        # if it started being used as a number.
        "-DSUITESPARSE=off",
        "-DCERES_GFLAGS_NAMESPACE=google",
    ],
    # The debug symbols for ceres are *pretty darn big*.  690M vs 15M
    # without.  Thus we always omit them and enforce optimizations.
    nocopts = "^-g$$",
    visibility = ["//visibility:public"],
    deps = [
        "@eigen//:eigen",
        "@glog//:glog",
    ],
)
